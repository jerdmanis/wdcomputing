# General numerics:
#
# Plot spin for various number of
# eigen vectors in one plot vs radius

using WDComputing
using Winston
import ColorSchemes

### Set system and form parameters
#include("Prepare_system.jl")

# System parameters

Lz = 4
Lr = 4/3
L = (Lr,Lr,Lz)
C = 5
C = (C,C,C)
II = (1,1,1)

Alpha = 1/sqrt(2).*(L./C).^(1/4)
homega = 1 ./ sqrt.(L.*C)

Eb = L.*II.^2 ./ 2
Ind = findmax(Eb)[2]
Eb = Eb[Ind]

Q = 1/2*Lz^1.5*II[Ind]^2*sqrt(C[Ind])
phi0 = II[Ind]*L[Ind]

II = II.*Alpha

@show Q
@show Eb
@show Alpha
@show phi0

area2 = 1*pi
R = sqrt(area2/pi)
theta0 = asin( -R/(Lz-Lr) )



Nphi = 101
N_A = 101

### Choose capacities
Ns = [20, 18, 16, 14, 12, 10]
#Ns = [12, 10]
Nn = length(Ns)
clrs = ColorSchemes.viridis



NEV = 2

phix = range(0, stop=1.0, length=Nphi)
phi = [phix[1], 0.0, 0.001]

A = zeros(2,2)*im

c = Array{Curve}(undef,Nn*2)

spin_minus = zeros(Nphi,3)
spin_plus = zeros(Nphi,3)
spin_plusminus = zeros(Complex{Float64},Nphi,3)


i = 1
for NN in Ns
    NN = (NN,NN,NN)   # Number of basis vectors
    AAA = AA(NN)
    sigz = sig_z(N)
    sigy = sig_y(N)
    sigx = sig_x(N)


    @info "Calculating $i/$Nn"

    for ss in 1:N_A
        global A, A1, A2

        phi[1] = phix[ss]/Alpha[1]
        p = ParametersJ(homega, II, (phi...,))

        minus,plus, = calcEV(p, NEV, NN)
        A = 2*pi*[ plus*AAA*plus' plus*AAA*minus' ; minus*AAA*plus' minus*AAA*minus' ]
        spin_minus[ss,:], spin_plus[ss,:], spin_plusminus[ss,:] = calcSpin(plus, minus, NN)
    end

    ### Create graph
    RR = string(Int(floor(get(clrs, i/Nn).r*256)), base=16)
    GG = string(Int(floor(get(clrs, i/Nn).g*256)), base=16)
    BB = string(Int(floor(get(clrs, i/Nn).b*256)), base=16)
    c[2*i-1] = Curve(phix, abs.(spin_plus[:,3])*2, color="0x$RR$GG$BB")
    c[2*i] = Curve(phix, abs.(spin_plusminus[:,3])*2, color="0x$RR$GG$BB")
#    if i==2 || i==Nn
        cc = NN[1]
        setattr(c[2*i], label="N=$cc^3")
#    end
    global i += 1
end




@info "Plotting"

plt = FramedPlot(xlabel="radius", ylabel="<i>σ_z</i>")
l = Legend(0.1, 0.7, c[2:2:2*Nn])
add(plt, c..., l)
plot(plt)
tmp = C[1]
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_spins_Ns_$tmp.pdf")
