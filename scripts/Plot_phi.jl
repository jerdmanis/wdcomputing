# General numerics:
#
# Plot superconducting phase 
# vs radius or offset


using WDComputing
using Printf



### Numerical calculations



@info "Setting parameters"

NEV = 2					# Number of eigenvectors

# Numerical parameters

# Choose for what values to make plots
Indx = 1
Nphi = 101

phix = range(0, stop=0.0007, length=Nphi) / Alpha[Indx]
phi = [phix[1], 0.0, 0.001]

# Prepare operators
global N = 20
N = (N,N,N)				# Number of basis vectors
include("Prepare_system.jl")


@info "Calculating..."

# Calculate phases
phixx = phi_x(N)
phizz = phi_z(N)
global phi_x_minus = zeros(Nphi)*im
global phi_x_plus = zeros(Nphi)*im
global phi_z_minus = zeros(Nphi)*im
global phi_z_plus = zeros(Nphi)*im
for ss in 1:Nphi
    @show ss
#    global phi_x_minus, phi_x_plus, minus, phi_z_plus
    phi[Indx] = phix[ss]
    p = ParametersJ(homega,II,(phi...,))
    minus, plus,  = calcEV(p, NEV, N)
    phi_x_minus[ss] = minus*phixx*minus' + phi[1]
    phi_x_plus[ss] = plus*phixx*plus' + phi[1]
    phi_z_minus[ss] = minus*phizz*minus' + phi[3]
    phi_z_plus[ss] = plus*phizz*plus' + phi[3]
end



@info "Plotting..."

using  Winston

# Automate plot title
if Indx==3
    ttl = @sprintf("Lz=%.2g, Lr=%.2g, Iz=%.2g, Ir=%.2g, C=%.2g, radius=%.2g, N=%d", L[3], L[2], II[3], II[2], C[3], phi[1], N[1])
    var = "offset"
else
    ttl = @sprintf("Lz=%.2g, Lr=%.2g, Iz=%.2g, Ir=%.2g, C=%.2g, offset=%.2g, N=%d", L[3], L[2], II[3], II[2], C[3], phi[3], N[1])
    var = "radius"
end


### phi_xz



# Plot superconducting phase
phix = phix*Alpha[Indx]
plt = plot(phix, real(phi_x_plus), "b", xlabel="\\phi^r_x", ylabel="\\phi_x", title="Quasiclassics")
hold(true)
plot(phix, real(phi_x_minus), "r--")
hold(false)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_phi_x($var).pdf")

plt2 = plot(phix, real(phi_z_plus), "b", xlabel="\\phi^r_x", ylabel="\\phi_z", title="Quasiclassics")
hold(true)
plot(phix, real(phi_z_minus), "r--")
hold(false)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_phi_z($var).pdf")




### Theoretical calculations

Nr = 201
Nz = 401
Nres = 41

Lz = 8.0
Lr = 1.3
Iz = 0.32
Ir = 0.24
C = 100
r = 0.3
p = Lz/Lr-1

r_max = abs(Lz*Iz^2-Lr*Ir^2)/Ir    # Condition for having two minima
r_max = min(r_max, 1.0)      # Condition for positive eigenvalues of Hessian matrix
rr = range(0, stop=r_max-0.01, length=Nres)


phi1 = zeros(Nres)
phi2 = zeros(Nres)
for m in 1:Nres
    phi1[m] = r/(1-Lr*Ir^2/(Lz*Iz^2))
    phi2[m] = -sqrt((Lz*Iz)^2 - (Ir/Iz*rr[m] / (1-Lr*Ir^2/(Lz*Iz^2)) )^2)
end

ttl = @sprintf("Lz=%.2g, Lr=%.2g, Iz=%.2g, Ir=%.2g", Lz, Lr, Iz, Ir)
var = "r"
x = rr

plt = plot(x, phi2, ylabel="<i>ϕ_z</i>", xlabel="<i>ϕ^r_x</i>")
savefig("$(dirname(dirname(@__FILE__)))/plots/phi_z(radius).pdf")
