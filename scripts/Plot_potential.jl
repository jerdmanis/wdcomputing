# For quasi-classical approach:
#
# Plot the classical energy 
# potential vs the x,z-phases
# in a 'heat map'

# Set system parameters
#I = (1,1,1)
Lz = 4;
Lr = 4/3;

# Set reservoir phases 
phiR_x = [0.5 1 2];
phiR_z = 0;

# Set domain
phiz_lim = 1.5;
phix_max = 3;
phix = range(0, length=100, stop=phix_max);
phiz = range(-phiz_lim, length=100, stop=phiz_lim);

# Plot
import Plots; Plots.pyplot();
using LaTeXStrings;
for i in 1:length(phiR_x)
    E(phi_x, phi_z) = (phi_x-phiR_x[i])^2 +(phi_z-phiR_z)^2 -sqrt(Lr*phi_x^2+Lz*phi_z^2);
#    if i==length(phiR_x)
#        plt = Plots.plot(phix, phiz, E, st=:contourf, levels=100, color=:RdBu, guidefontsize=22, tickfontsize=22);
#    else
        plt = Plots.plot(phix, phiz, E, st=:contourf, levels=100, color=:RdBu, legend=nothing, guidefontsize=22, tickfontsize=22);   # ticks=false, size=(300, 800)
#        if i==1
            Plots.ylabel!(L"\phi_z");
#        end
#    end
    Plots.xlabel!(L"\phi_x");
    Plots.savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_Pot_$i.pdf")
end

