# General numerics:
#
# Plot rotation angle for various
# number of basis states in one plot
# vs radious or offset


using WDComputing
using Winston
import ColorSchemes

### Set system and form parameters
#include("Prepare_system.jl")

# System parameters

Lz = 4
Lr = 4/3
L = (Lr,Lr,Lz)
C = 20
C = (C,C,C)
II = (1,1,1)

Alpha = 1/sqrt(2).*(L./C).^(1/4)
homega = 1 ./ sqrt.(L.*C)

Eb = L.*II.^2 ./ 2
Ind = findmax(Eb)[2]
Eb = Eb[Ind]

Q = 1/2*Lz^1.5*II[Ind]^2*sqrt(C[Ind])
phi0 = II[Ind]*L[Ind]

II = II.*Alpha

@show Q
@show Eb
@show Alpha
@show phi0

area2 = 1*pi
R = sqrt(area2/pi)
theta0 = asin( -R/(Lz-Lr) )



Nphi = 101
N_A = 101

### Choose capacities
Ns = [20, 18, 16, 14, 12, 10]
#Ns = [12, 10]
Nn = length(Ns)+1
clrs = ColorSchemes.viridis

### Set circle parametrisation as function of A
rrr(phi,A) = sqrt(A/pi)   # b = sqrt(AA/pi/f)

@info "Calculating 1/$Nn"

### Calculate evolution matrix and its exponent and the angle
area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=0, phi_max=2*pi, Nphi=Nphi, multiplier=1, A_max=pi*(Lz-Lr)^2-0.01, N_A=N_A)

### Create graph
c = Array{Curve}(undef,Nn)
c[1] = Curve(area, 1 .-abs.(delta)/2/pi, color="red", linetype="longdashed")
setattr(c[1], label="QC analytics")



NEV = 2

phix = sqrt.(area/pi)/Alpha[1]
phi = [phix[1], 0.0, 0.001]

alpha = zeros(N_A)*im
A = zeros(2,2)*im

i = 2
for NN in Ns
    NN = (NN,NN,NN)   # Number of basis vectors
    AAA = AA(NN)

    @info "Calculating $i/$Nn"

    for ss in 1:N_A
        global A, A1, A2

        phi[1] = phix[ss]
        p = ParametersJ(homega, II, (phi...,))

        minus,plus, = calcEV(p, NEV, NN)
        A = 2*pi*[ plus*AAA*plus' plus*AAA*minus' ; minus*AAA*plus' minus*AAA*minus' ]
        alpha[ss], = CalcRotation(A)
    end

    ### Create graph
    RR = string(Int(floor(get(clrs, i/Nn).r*256)), base=16)
    GG = string(Int(floor(get(clrs, i/Nn).g*256)), base=16)
    BB = string(Int(floor(get(clrs, i/Nn).b*256)), base=16)
    c[i] = Curve(area, 1 .-abs.(alpha)/2/pi, color="0x$RR$GG$BB")
#    if i==2 || i==Nn
        cc = NN[1]
        setattr(c[i], label="N=$cc^3")
#    end
    global i += 1
end




@info "Plotting"

plt = FramedPlot(yrange=(0,1.0), show=true, xlabel="<i>A</i>", ylabel="<i>α</i>/2π")
#l = Legend(0.1, 0.9, [c[1], c[2], c[Nn]])
l = Legend(0.1, 0.9, c)
add(plt, c..., l)
plot(plt)
tmp = C[1]
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_circle_Ns_$tmp.pdf")
