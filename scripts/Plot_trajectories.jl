# For quasi-classical approach:
#
# Plot rotation angle for various
# trajecotries in one plot vs area

using WDComputing
using Winston

### Set system and form parameters
include("Prepare_system.jl")

Nphi = 101
N_A = 101

### Set ellipse parametrisation as function of A
f = 3
e = sqrt(1-f^-2)
rrr(phi,A) = sqrt(A/pi/f) / sqrt( 1-(e*cos(phi))^2 )   # b = sqrt(AA>
phi_min = 0
phi_max = 2*pi

### Calculate evolution matrix and its exponent and the angle
multiplier = 1

area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=0, phi_max=2*pi, Nphi=Nphi, multiplier=1, A_max=pi*(Lz-Lr)^2/f-0.1, N_A=N_A)
#include("QC_Traj.jl")

### Create graph
c1 = Curve(area, 1 .-abs.(delta)/2/pi, color="red")
setattr(c1, label="Ellipse")



### Set square parametrisation as function of A
rrr(phi,A) = sqrt(A)/2/cos(phi)   # R = sqrt(A)

### Calculate evolution matrix and its exponent and the angle
#include("QC_Traj.jl")
area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=-pi/4, phi_max=pi/4, Nphi=Nphi, multiplier=4, A_max=2*(Lz-Lr)^2-0.01, N_A=N_A)

### Create graph
c2 = Curve(area, 1 .-abs.(delta)/2/pi, color="blue")
setattr(c2, label="Square")



### Set circle parametrisation as function of A
rrr(phi,A) = sqrt(A/pi)   # b = sqrt(AA/pi/f)

### Calculate evolution matrix and its exponent and the angle
#include("QC_Traj.jl")
area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=0, phi_max=2*pi, Nphi=Nphi, multiplier=1, A_max=pi*(Lz-Lr)^2-0.01, N_A=N_A)

### Create graph
c3 = Curve(area, 1 .-abs.(delta)/2/pi, color="green")
setattr(c3, label="Circle")



@info "Plotting"

plt = FramedPlot(yrange=(0,0.5), show=true, xlabel="<i>A</i>", ylabel="(2π-<i>δ</i>)/2π")
l = Legend(0.1, 0.7, [c1, c2, c3])
add(plt, c1, c2, c3, l)
plot(plt)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_Trajectories.pdf")
