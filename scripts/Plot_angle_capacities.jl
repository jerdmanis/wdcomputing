# For quasi-classical approach:
# 
# Plot rotation angle for various 
# capacities in one plot vs area
# and compare with general numerics
# plot


using WDComputing
using Winston
import ColorSchemes

### Set system and form parameters
include("Prepare_system.jl")

Nphi = 101
N_A = 101

### Choose capacities
Cs = [5 10 20]
#Cs = [100 500]
Nc = length(Cs)+1
clrs = ColorSchemes.viridis

### Set circle parametrisation as function of A
rrr(phi,A) = sqrt(A/pi)   # b = sqrt(AA/pi/f)

@info "Calculating 1/$Nc"

### Calculate evolution matrix and its exponent and the angle
area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=0, phi_max=2*pi, Nphi=Nphi, multiplier=1, A_max=pi*(Lz-Lr)^2-0.01, N_A=N_A)

### Create curve
c = Array{Curve}(undef,Nc)
c[1] = Curve(area, 1 .-abs.(delta)/2/pi, color="red", linetype="longdashed")
setattr(c[1], label="QC analytics")



NEV = 2
NN = 20
NN = (NN,NN,NN)   # Number of basis vectors

phix = sqrt.(area/pi)
phi = [phix[1], 0.0, 0.001]
AAA = AA(NN)

alpha = zeros(N_A)*im
A = zeros(2,2)*im

i = 2
for C in Cs
    @info "Calculating $i/$Nc"

    C = (C,C,C)
    Alpha = 1/sqrt(2).*(L./C).^(1/4)
    II = (1,1,1) .*Alpha
    homega = 1 ./ sqrt.(L.*C)
    RR = Int(floor( 99*(1-i/Nc) ))

    for ss in 1:N_A
        global A, A1, A2

        phi[1] = phix[ss]/Alpha[1]
        p = ParametersJ(homega, II, (phi...,))

        minus,plus, = calcEV(p, NEV, NN)
        A = 2*pi*[ plus*AAA*plus' plus*AAA*minus' ; minus*AAA*plus' minus*AAA*minus' ]
        alpha[ss], = CalcRotation(A)
    end

    ### Create graph
    RR = string(Int(floor(get(clrs, i/Nc).r*256)), base=16)
    GG = string(Int(floor(get(clrs, i/Nc).g*256)), base=16)
    BB = string(Int(floor(get(clrs, i/Nc).b*256)), base=16)
    c[i] = Curve(area, 1 .-abs.(alpha)/2/pi, color="0x$RR$GG$BB")
#    if i==2 || i==Nc    
        cc = C[1]
        setattr(c[i], label="C=$cc")
#    end
    global i += 1
end




@info "Plotting"

plt = FramedPlot(yrange=(0,1.0), show=true, xlabel="<i>A</i>", ylabel="<i>α</i>/2π")
#l = Legend(0.1, 0.9, [c[1], c[2], c[Nc]])
l = Legend(0.1, 0.9, c)
add(plt, c..., l)
plot(plt)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_circle_capacities.pdf")
