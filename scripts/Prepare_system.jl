# System parameters

Lz = 4
Lr = 4/3
L = (Lr,Lr,Lz)
C = (20,20,20)
II = (1,1,1)


# Dependent parameters

Alpha = 1/sqrt(2).*(L./C).^(1/4)
homega = 1 ./ sqrt.(L.*C)

Eb = L.*II.^2 ./ 2
Ind = findmax(Eb)[2]
Eb = Eb[Ind]
Q = 1/2*Lz^1.5*II[Ind]^2*sqrt(C[Ind])
phi0 = II[Ind]*L[Ind]

# Scale II for use in numerical function
II = II.*Alpha

@show Q
@show Eb
@show phi0


### Form parameters
# Area may not be too large,
# because if radius is too large,
# wave functions are not localised.
area2 = 1*pi
R = sqrt(area2/pi)
theta0 = asin( -R/(Lz-Lr) )


