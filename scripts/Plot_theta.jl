# For quasi-classical approach:
# Plot the angle theta of the minima
# with the z-axis vs scaled radius


using Winston


plt = fplot(rho -> asin(rho), [0 1], "red")
xlabel("<i>ρ</i>")
ylabel("<i>θ</i>")

savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_theta(r).pdf")

