# For quasi-classical approach:
#
# Plot rotation angle for various
# circles in one plot vs area


using WDComputing
using Winston

### Set system and form parameters
include("Prepare_system.jl")

Nphi = 101
Nt = 101
N_A = 101

### Set circle parametrisation as function of A
rrr(phi,A) = sqrt(A/pi)   # b = sqrt(AA/pi/f)

### Calculate evolution matrix and its exponent and the angle
area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=0, phi_max=2*pi, Nphi=Nphi, multiplier=1, A_max=pi*(Lz-Lr)^2-0.01, N_A=N_A)

### Create graph
c1 = Curve(area, 1 .-abs.(delta)/2/pi, color="red")
setattr(c1, label="Centred")



### Set intermediate circle parametrisation as function of A
# 1*sqrt(1+2*0.5* (-2*0.5*tan(phi)^2-sqrt( 4*0.5^2*tan(phi)^4-4*(tan(phi)^2+1)*(0.5^2*tan(phi)^2-1) )) /2/(tan(phi)^2+1) +0.5^2)
a_R = 1-1e-6
sqrtD(phi) = (phi>pi/2 && phi<3*pi/2 ? -1 : 1) * sqrt( 4*a_R^2*tan(phi)^4-4*(tan(phi)^2+1)*(a_R^2*tan(phi)^2-1) )
costheta(phi) = (-2*a_R*tan(phi)^2+sqrtD(phi)) /2/(tan(phi)^2+1)
rrr(phi,A) = sqrt(A/pi)* sqrt(1 + 2*a_R*costheta(phi) + a_R^2)   # R = sqrt(A/pi)

### Calculate evolution matrix and its exponent and the angle
area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=0, phi_max=2*pi, Nphi=Nphi, multiplier=1, A_max=pi/4*(Lz-Lr +(1-a_R)*(Lz-Lr)/(a_R+1) )^2-0.01, N_A=N_A)

### Create graph
c2 = Curve(area, 1 .-abs.(delta)/2/pi, color="green")
setattr(c2, label="Shifted, enclosing centre")



### Set circle through centre parametrisation as function of A
rrr(phi,A) = 2*sqrt(A/pi)*cos(phi)^2   # R = sqrt(A/pi)

### Calculate evolution matrix and its exponent and the angle
area, delta = QC_Traj(r=rrr, theta0=theta0, Lz_Lr=Lz-Lr, phi_min=-pi/2, phi_max=pi/2, Nphi=Nphi, multiplier=1, A_max=pi/4*(Lz-Lr)^2-0.01, N_A=N_A)

### Create graph
c3 = Curve(area, 1 .-abs.(delta)/2/pi, color="blue")
setattr(c3, label="Through center")



### Set circle not enclosing centre parametrisation as function of A
ppphi(t,A) = atan(sin(t)/(1+1e-6+cos(t)))
rrr(t,A) = sqrt(A/pi)*sqrt((1+1e-6+cos(t))^2+(sin(t))^2)   # R = sqrt(A/pi)

### Calculate evolution matrix and its exponent and the angle
area, delta = QC_Traj_param(r=rrr, phi=ppphi, theta0=theta0, Lz_Lr=Lz-Lr, t_min=0, t_max=2*pi, Nt=Nt, multiplier=1, A_max=pi/4*(Lz-Lr)^2-0.01, N_A=N_A)

### Create graph
c4 = Curve(area, 1 .-abs.(delta)/2/pi, color="violet")
setattr(c4, label="Not enclosing centre")



@info "Plotting"

plt = FramedPlot(yrange=(0,1.0), show=true, xlabel="<i>A</i>", ylabel="(2π-<i>δ</i>)/2π")
l = Legend(0.1, 0.9, [c4, c3, c2, c1])
add(plt, c1, c2, c3, c4, l)
plot(plt)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_Circles.pdf")
