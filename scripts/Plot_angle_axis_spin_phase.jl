# General numerics:
# 
# Plot the energies of the system
# Plot the angle of rotation over the 
# Bloch sphere.
# Plot the axis of rotation over the 
# Bloch sphere.
# Plot the x,z-components of the spin
# and phase together.
# Plot the x,y,z-components of the spin.
# Plot the x,y,z-components of the phase.
#
# All as function of radius or offset


using WDComputing
using Printf


### Numerics

@info "Setting parameters"

# Number of eigenvectors
NEV = 2;

# Choose for what values to make plots
# Indx=1 is plotting vs radius, Indx=3 is vs offset
Indx = 1
Nphi = 101

# Prepare operators and set variables
N = 20
N = (N,N,N)   # Number of basis vectors
include("Prepare_system.jl")


if Indx==1		# radius on x-axis
	phix = range(0, stop=1.0, length=Nphi)
	phi = [phix[1], 0.0, 0.001]
elseif Indx==3	# offset on x-axis
	phix = range(0, stop=0.001, length=Nphi)
	phi = [0.1, 0.0, phix[1]]
end
phix = phix/Alpha[Indx]



@info "Calculating"

### Calculate

AAA = AA(N)
#Lz = L_z(N)
#sigz = sig_z(N)

A = zeros(2,2)*im
#A1 = zeros(2,2)*im
#A2 = zeros(2,2)*im
alpha = zeros(Nphi)*im #; alpha1 = zeros(Nphi)*im; alpha2 = zeros(Nphi)*im
beta = zeros(Nphi)*im
nz = zeros(Nphi)*im #; nz1 = zeros(Nphi)*im; nz2 = zeros(Nphi)*im
nr = zeros(Nphi)*im #; nr1 = zeros(Nphi)*im; nr2 = zeros(Nphi)*im

sigz = sig_z(N)
sigy = sig_y(N)
sigx = sig_x(N)
spin_minus = zeros(Nphi,3)
spin_plus = zeros(Nphi,3)
spin_plusminus = zeros(Complex{Float64},Nphi,3)
	
phixx = phi_x(N)
phiyy = phi_y(N)
phizz = phi_z(N)
global phi_x_minus = zeros(Nphi)*im
global phi_x_plus = zeros(Nphi)*im
global phi_z_minus = zeros(Nphi)*im
global phi_z_plus = zeros(Nphi)*im
global phi_y_minus = zeros(Nphi)*im
global phi_y_plus = zeros(Nphi)*im

# Calculate unitary evolution for one cycle
for ss in 1:Nphi
    @show ss

    global A
    
    phi[Indx] = phix[ss]
    p = ParametersJ(homega,II,(phi...,))
    
    minus,plus, = calcEV(p, NEV, N)
    
    # Calculate 2x2 matrix A as in U=exp^(-iA) in basis {|+>,|->}
    A = 2*pi*[ plus*AAA*plus' plus*AAA*minus' ; minus*AAA*plus' minus*AAA*minus' ]
    #A1 = 2*pi*[ plus*Lz*plus' plus*Lz*minus' ; minus*Lz*plus' minus*Lz*minus' ]
    #A2 = 2*pi*[ plus*sigz*plus' plus*sigz*minus' ; minus*sigz*plus' minus*sigz*minus' ]
    
    # Calculate rotation angles (for L_z and sigma/2 together)
    alpha[ss],nr[ss],nz[ss] = CalcRotation(A)
    beta[ss] = tan(nr[ss]/nz[ss])

    spin_minus[ss,:], spin_plus[ss,:], spin_plusminus[ss,:] = calcSpin(plus, minus, N)

    phi_x_minus[ss] = minus*phixx*minus' + phi[1]
    phi_x_plus[ss] = plus*phixx*plus' + phi[1]
    phi_z_minus[ss] = minus*phizz*minus' + phi[3]
    phi_z_plus[ss] = plus*phizz*plus' + phi[3]
    phi_y_minus[ss] = minus*phiyy*minus' + phi[2]
    phi_y_plus[ss] = plus*phiyy*plus' + phi[2]

    # Verify unitarity
    U = exp(-im*A)
    printmat(U*U')
end

phi_minus = sqrt.(abs.(phi_x_minus.^2 .+phi_y_minus.^2 .+phi_z_minus.^2))
phi_plus = sqrt.(abs.(phi_x_plus.^2 .+phi_y_plus.^2 .+phi_z_plus.^2))




@info "Plotting"

using  Winston

# Automate plot title
if Indx==3
    ttl = @sprintf("Lz=%2g, Lr=%2g, Iz=%2g, Ir=%2g, C=%2g, radius=%2g, N=%d", L[3], L[2], II[3], II[2], C[3], phi[1], N[1]);
    var = "offset";
else
    ttl = @sprintf("Lz=%2g, Lr=%2g, Iz=%2g, Ir=%2g, C=%2g, offset=%2g, N=%d", L[3], L[2], II[3], II[2], C[3], phi[3], N[1]);
    var = "radius";
end

phix = phix*Alpha[Indx]



# Plot rotation angle

plt1 = FramedPlot(xlabel=var, ylabel="<i>α/2π</i>")
c1 = Curve(phix, 1 .-abs.(alpha)/2/pi, color="blue")
add(plt1, c1)
plot(plt1)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_alpha($var).pdf")



# Plot rotation axis angle

plt2 = FramedPlot(yrange=(0,1), xlabel=var, ylabel="β/2π");
c2 = Curve(phix, abs.(beta)/2*pi, color="blue");
add(plt2, c2);
plot(plt2);
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_axis($var).pdf");



### Plot spins

plt3 = FramedPlot(xlabel=var, ylabel="quasi-spin")
c4 = Curve(phix, abs.(spin_minus[:,1]), color="red")
setattr(c4, label="&lt;-|<i>σ_x</i>|-&gt;")
c5 = Curve(phix, abs.(spin_plus[:,1]), color="blue", linetype="longdashed")
setattr(c5, label="&lt;+|<i>σ_x</i>|+&gt;")
c6 = Curve(phix, abs.(spin_plusminus[:,1]), color="green")
setattr(c6, label="&lt;+|<i>σ_x</i>|-&gt;")
l = Legend(0.7, 0.4, [c4, c5, c6])
add(plt3, c4, c5, c6, l)
plot(plt3);
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_spin_x($var).pdf")

plt4 = FramedPlot(xlabel=var, ylabel="quasi-spin")
c7 = Curve(phix, abs.(spin_minus[:,2]), color="red")
setattr(c7, label="&lt;-|<i>σ_y</i>|-&gt;")
c8 = Curve(phix, abs.(spin_plus[:,2]), color="blue", linetype="longdashed")
setattr(c8, label="&lt;+|<i>σ_y</i>|+&gt;")
c9 = Curve(phix, abs.(spin_plusminus[:,2]), color="green")
setattr(c9, label="&lt;+|<i>σ_y</i>|-&gt;")
l = Legend(0.1, 0.4, [c7, c8, c9])
add(plt4, c7, c8, c9, l)
plot(plt4)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_spin_y($var).pdf")

plt5 = FramedPlot(xlabel=var, ylabel="quasi-spin")
c10 = Curve(phix, abs.(spin_minus[:,3]), color="blue")
setattr(c10, label="&lt;-|<i>σ_z</i>|-&gt;")
c11 = Curve(phix, abs.(spin_plus[:,3]), color="red", linetype="longdashed")
setattr(c11, label="&lt;+|<i>σ_z</i>|+&gt;")
c12 = Curve(phix, abs.(spin_plusminus[:,3]), color="green")
setattr(c12, label="&lt;+|<i>σ_z</i>|-&gt;")
l = Legend(0.1, 0.55, [c10, c11, c12])
add(plt5, c10, c11, c12, l)
plot(plt5)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_spin_z($var).pdf")



# Plot phases

plt6 = FramedPlot(xlabel=var, ylabel="E(<i>ϕ_x</i>)")
c13 = Curve(phix, real.(phi_x_plus), color="blue")
setattr(c13, label="|+&gt;")
c14 = Curve(phix, real.(phi_x_minus), color="red", linetype="longdashed")
setattr(c14, label="|-&gt;")
l = Legend(0.1, 0.55, [c13, c14])
add(plt6, c13, c14, l)
plot(plt6)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_phi_x($var).pdf")

plt7 = FramedPlot(xlabel=var, ylabel="E(<i>ϕ_z</i>)")
c15 = Curve(phix, real.(phi_z_plus), color="blue")
setattr(c15, label="|+&gt;")
c16 = Curve(phix, real.(phi_z_minus), color="red")
setattr(c16, label="|-&gt;")
l = Legend(0.1, 0.55, [c15, c16])
add(plt7, c15, c16, l)
plot(plt7)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_phi_z($var).pdf")



# Plot phases with spins

plt8 = FramedPlot(yrange=(0,1), show=true, xlabel=var)
c17 = Curve(phix, abs.(phi_x_minus./phi_minus), color="blue")
setattr(c17, label="&lt;-|<i>ϕ_x</i>|-&gt;/|ϕ|")
c18 = Curve(phix, abs.(phi_x_plus./phi_plus), color="red", linetype="longdashed")
setattr(c18, label="&lt;+|<i>ϕ_x</i>|+&gt;/|ϕ|")
c19 = Curve(phix, abs.(spin_minus[:,1]), color="green")
setattr(c19, label="-&lt;-|<i>σ_x</i>|-&gt;")
c20 = Curve(phix, abs.(spin_plus[:,1]), color="black", linetype="longdashed")
setattr(c20, label="-&lt;+|<i>σ_x</i>|+&gt;")
l = Legend(0.1, 0.9, [c17, c18, c19, c20])
add(plt8, c17, c18, c19, c20, l)
plot(plt8)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_spin_phi_x($var).pdf")

plt9 = FramedPlot(yrange=(0,1), show=true, xlabel=var)
c21 = Curve(phix, abs.(phi_z_minus./phi_minus), color="blue")
setattr(c21, label="-&lt;-|<i>ϕ_z</i>|-&gt;/|ϕ^0|")
c22 = Curve(phix, abs.(phi_z_plus./phi_plus), color="red", linetype="longdashed")
setattr(c22, label="&lt;+|<i>ϕ_z</i>|+&gt;/|ϕ^0|")
c23 = Curve(phix, abs.(spin_minus[:,3]), color="green")
setattr(c23, label="&lt;-|<i>σ_z</i>|-&gt;")
c24 = Curve(phix, abs.(spin_plus[:,3]), color="black", linetype="longdashed")
setattr(c24, label="-&lt;+|<i>σ_z</i>|+&gt;")
l = Legend(0.1, 0.3, [c21, c22, c23, c24])
add(plt9, c21, c22, c23, c24, l)
plot(plt9)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_spin_phi_z($var).pdf")

plt10 = FramedPlot(yrange=(0,1), show=true, xlabel=var)
c25 = Curve(phix, abs.(phi_y_minus./phi_minus), color="blue")
setattr(c25, label="&lt;-|<i>ϕ_y</i>|-&gt;/|ϕ^0|")
c26 = Curve(phix, abs.(phi_y_plus./phi_plus), color="red", linetype="longdashed")
setattr(c26, label="&lt;+|<i>ϕ_y</i>|+&gt;/|ϕ^0|") 
c27 = Curve(phix, abs.(spin_minus[:,2]), color="green")
setattr(c27, label="&lt;-|<i>σ_y</i>|-&gt;")
c28 = Curve(phix, abs.(spin_plus[:,2]), color="black", linetype="longdashed")
setattr(c28, label="&lt;+|<i>σ_y</i>|+&gt;")
l = Legend(0.1, 0.4, [c25, c26, c27, c28])
add(plt10, c25, c26, c27, c28, l)
plot(plt10)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_spin_phi_y($var).pdf")

