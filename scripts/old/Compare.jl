### System parameters
Lz = 4
Lr = 4/3

### Ellipse parameters
b = 1
a = 3
e = (a-b)/a

### Ellipse parametrisation
### Take phi=t 
r(phi) = b / sqrt( 1-(e*cos(phi))^2 )
theta(phi) = asin( -r(phi)/(Lz-Lr) )

### Basis transformation matrices
P(phi) = [ cos(theta(phi)/2) sin(theta(phi)/2); -sin(theta(phi)/2) cos(theta(phi)/2) ]
P_inv(phi) = [ cos(theta(phi)/2) -sin(theta(phi)/2); sin(theta(phi)/2) cos(theta(phi)/2) ]

### Connections, where M3 should be equal to M2
M1(phi) = im/2* [ -cos(theta(phi)) 0; 0 cos(theta(phi)) ]
M2(phi) = im/2*cos(theta(phi))* [ -cos(theta(phi)) sin(theta(phi)); sin(theta(phi)) cos(theta(phi)) ]
M3(phi) = P(phi)*M1(phi)*P_inv(phi)

### Integrate over a cycle
dphi = 0.01
I1 = [0 0; 0 0]
I2 = [0 0; 0 0]
for phi in range(0, 2*pi, step=dphi)
    global I1 += dphi*M1(phi)
    global I2 += dphi*M2(phi)
end

### Evolution operators
U1 = P(0)*exp(I1)*P_inv(0)
U2 = exp(I2)


