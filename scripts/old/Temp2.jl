using WDComputing
using Winston

### Set system and form parameters
include("SystemQC.jl")

### Set circle parametrisation as function of A
r(phi,A) = sqrt(A/pi)   # b = sqrt(AA/pi/f)
phi_min = 0
phi_max = 2*pi

@info "Calculating 1"

### Calculate evolution matrix and its exponent and the angle
multiplier = 1
include("QC_Traj.jl")

### Create graph
c1 = Curve(area, 1 .-abs.(delta)/2/pi, color="red")
setattr(c1, label="QC analytics")




NEV = 2
NN = 18
NN = (NN,NN,NN)   # Number of basis vectors
Lz = 4
Lr = 4/3
L = (Lr,Lr,Lz)
C = 100
C = (C,C,C)
II = 1/sqrt(2).*(0.2 ./C).^(1/4)
homega = 1 ./ sqrt.(L.*C)

phix = sqrt.(area./pi)
phi = [phix[1], 0.0, 0.13]
AAA = AA(NN)

@info "Calculating 2"

alpha = zeros(N)*im
A = zeros(2,2)*im
for ss in 1:N
    global A, A1, A2

    phi[1] = sqrt(area[ss]/pi)
    p = ParametersJ(homega, II, (phi...,))

    minus,plus, = calcEV(p, NEV, NN)
    A = 2*pi*[ plus*AAA*plus' plus*AAA*minus' ; minus*AAA*plus' minus*AAA*minus' ]
    alpha[ss], = CalcRotation(A)
end

### Create graph
c2 = Curve(area, 1 .-abs.(alpha)/2/pi, color="blue")
c = phi[3]
setattr(c2, label="Numerics offset=$c")



@info "Calculating 3"

phi = [phix[1], 0.0, 0.15]

A = zeros(2,2)*im
for ss in 1:N
    global A, A1, A2

    phi[1] = sqrt(area[ss]/pi)
    p = ParametersJ(homega, II, (phi...,))

    minus,plus, = calcEV(p, NEV, NN)
    A = 2*pi*[ plus*AAA*plus' plus*AAA*minus' ; minus*AAA*plus' minus*AAA*minus' ]
    alpha[ss], = CalcRotation(A)
end

### Create graph
c3 = Curve(area, 1 .-abs.(alpha)/2/pi, color="green")
c = phi[3]
setattr(c3, label="Numerics offset=$c")



@info "Plotting"

plt = FramedPlot(yrange=(0,0.6), show=true, xlabel="<i>A</i>", ylabel="<i>α</i>/2π")
l = Legend(0.1, 0.78, [c1, c2, c3])
add(plt, c1, c2, c3, l)
plot(plt)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_circle_temp2.pdf")
