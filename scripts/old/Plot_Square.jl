using WDComputing

### Set system and form parameters
include("SystemQC.jl")

### Calculat new ellipse parametrisation
### Take phi=t
r(phi,A) = sqrt(A)/cos(phi)   # R = sqrt(A)

phi_min = -pi/4
phi_max = pi/4
multiplier = 4

### Calculate evolution matrix and its exponent
include("QC_Traj.jl")



@info "Plotting"
using  Winston

### Plot rotation angle
plt1 = plot(area, abs.(alpha)/pi, "b", xlabel="<i>A</i>", ylabel="<i>α/π</i>")
ylim(0, 2)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_Square_alpha.pdf")

### Plot rotation axis
plt2 = FramedPlot(yrange=(0,1), show=true, xlabel="<i>A</i>", ylabel="<i>n_.</i>")
c1 = Curve(area, abs.(nz), color="red")
setattr(c1, label="<i>n_z</i>")
c2 = Curve(area, abs.(nr), color="blue")
setattr(c2, label="<i>n_r</i>")
l = Legend(0.1, 0.7, [c1, c2])
add(plt2, c1, c2, l)
plot(plt2)
savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_Square_n.pdf")

