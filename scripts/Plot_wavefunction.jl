# General Numerics:
#
# Plot the wavefunction vs phase in the
# z-direction, showing the minima move
# towards each other


using WDComputing
using Printf


@info "Setting parameters"

# Number of eigenvectors
NEV = 2
# Number of basis vectors
N = 20
N = (N, N, N)
Nres = 101

include("Prepare_system.jl")

Phi_z = range(-4, stop=4, length=Nres)/Alpha[3]
Pmin = zeros(Nres)
Pplus = zeros(Nres)



using Winston
for phi_r in [0 1 2 2.2 2.6]
#for phi_r in [2.2 2.4 2.6 2.8]
    phi = (phi_r/Alpha[1], 0.0, 1e-3/Alpha[3])
    @info "Calculating"
    for n in 1:Nres
        p = ParametersJ(homega,II,(phi...,))
        minus, plus, = calcEV(p, NEV, N)
        Pmin[n] = prob(Phi_z[n], minus, phi[3], N...)   # As function now of Phi_z=phi_z/alpha_z
        Pplus[n] = prob(Phi_z[n], plus, phi[3], N...)
    end
    @info "Plotting"
    plt = FramedPlot(yrange=(0,0.85), ylabel="<i>P</i>", xlabel="<i>ϕ_z</i>");
    c1 = Curve(Phi_z*Alpha[3], Pmin, color="blue");
    setattr(c1, label="|-&gt;");
    c2 = Curve(Phi_z*Alpha[3], Pplus, color="red");
    setattr(c2, label="|+&gt;");
    l = Legend(0.45, 0.8, [c1, c2]);
    add(plt, c1, c2, l);
    plot(plt);
    savefig("$(dirname(dirname(@__FILE__)))/plots/Plot_Prob(phi_z)_$phi_r.pdf")
end


#    plt = plot(Phi_z*Alpha[3], Pmin, "b", ylabel="<i>P</i>", xlabel="<i>ϕ^r_z</i>")
#    hold(true)
#    plot(Phi_z*Alpha[3], Pplus, "r")
#    hold(false)
#    ylim(0, 0.8)

