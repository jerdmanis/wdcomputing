# WDComputing

Exploring the Weyl Disk for geometric quantum computation.  


To run the code, the following steps need to be taken

    + Download Julia from `https://julialang.org/` and install.
    + Enter the project Git folder
    + Run `julia` from the command line in this folder which opens a REPL (like interpreter but works as "interactive compiler").
    + In the REPL type `]activate .`, which will put you in the environment defined by the files `Project.toml` and `Manifest.toml` files. (The `]` puts you in Pkg shell. You can exit it with backspace)
    + Now run in this Pkg shell `instantiate`. That will install dependencies defined in `Project.toml` and the correct versions in `Manifest.toml` (both are machine written).
    + And lastly, run `dev .` which will add the WDcomputing module to the search path. Now exit the Pkg environment with backspace.
    + To run any script enter the `scripts` folder run in the REPL `include("filename.jl")`, which will run the code and produce the plots in the 'plots' folder
