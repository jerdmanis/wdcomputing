
### Structure for Hamiltonian parameters
struct ParametersJ
    hw::Tuple{Float64,Float64,Float64}
    I::Tuple{Float64,Float64,Float64}
    phi::Tuple{Float64,Float64,Float64}
end

### Define Pauli matrices
const sigmax = [0 1;1 0] 
const sigmay = [0 -1im;1im 0]
const sigmaz = [1 0;0 -1] 

### Define delta functions
δ(m,n) = m==n ? 1 : 0



### Create a matrix from a function of harmonic
### oscillator indices for x,y,z-directions.
function FillOperator(Op::Function,Nx::Int,Ny::Int,Nz::Int)

    Opm = Array{Complex}(undef,0)
    Iv = Array{Int}(undef,0)
    Jv = Array{Int}(undef,0)
    
    s(l,m,n,a) = 2*(Ny+1)*(Nz+1)*l+2*(Nz+1)*m+2*n+a
    for l in 0:Nx    
        for m in 0:Ny
            for n in 0:Nz
                for a in 1:2
                    for l_ in 0:Nx    
                        for m_ in 0:Ny
                            for n_ in 0:Nz
                                for a_ in 1:2
                                    sbra = s(l_,m_,n_,a_)
                                    sket = s(l,m,n,a)
                                    
                                    Opi = Op(l_,m_,n_,a_,l,m,n,a)
                                    if Opi!=0 || sbra==sket
                                        push!(Iv,sbra)
                                        push!(Jv,sket)
                                        push!(Opm,Opi)
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    return sparse(Iv,Jv,Opm)
end



### The Hamiltonian operator function
function H(l_,m_,n_,a_,l,m,n,a,p::ParametersJ)

    hw,I,phi = p.hw,p.I,p.phi

    sigma = (sigmax[a_,a],sigmay[a_,a],sigmaz[a_,a]) 
    bc = (sqrt(l+1)*δ(l_,l+1), sqrt(m+1)*δ(m_,m+1), sqrt(n+1)*δ(n_,n+1))
    ba = (sqrt(l)*δ(l_,l-1),sqrt(m)*δ(m_,m-1),sqrt(n)*δ(n_,n-1))

    E = δ(l_,l)*δ(m_,m)*δ(n_,n)

    s::Complex{Float64} = 0

    s += sum(hw.*(l+1/2,m+1/2,n+1/2).*E.*δ(a_,a))
    s += sum(sigma.*I.*(bc.+ba).*(δ(m_,m)*δ(n_,n),δ(l_,l)*δ(n_,n),δ(l_,l)*δ(m_,m)))
    s += sum(sigma.*I.*phi)*E
    return s
end



### The phase operator function
function Phi(l_,m_,n_,a_,l,m,n,a,xyz)

    if xyz==1
	bc = sqrt(l+1)*δ(l_,l+1);
	ba = sqrt(l)*δ(l_,l-1);
	s = (bc+ba)*δ(m_,m)*δ(n_,n)*δ(a_,a)
    elseif xyz==2
	bc = sqrt(m+1)*δ(m_,m+1);
	ba = sqrt(m)*δ(m_,m-1);
	s = (bc+ba)*δ(l_,l)*δ(n_,n)*δ(a_,a)
    elseif xyz==3
	bc = sqrt(n+1)*δ(n_,n+1);
	ba = sqrt(n)*δ(n_,n-1);
	s = (bc+ba)*δ(l_,l)*δ(m_,m)*δ(a_,a)
    end

    return s
end



### Calculate 3D array for L_z named P

### Two functions to calculate Fn
function HermiteCoefficients(N)
    a = zeros(Int128,N,N+1)
    a[1,1]=1
    for n in 1:(N-1)
        a[n+1,1]=-a[n,1+1]
        for k in 2:(N-1)
            a[n+1,k]=2*a[n,k-1] - k*a[n,k+1]
        end
        a[n+1,N]=2*a[n,N-1]
    end
    return a
end
function Polynomial(a, x)
    H = zeros(length(x), 1);
    for na in 1:length(a)
        H += a[na]*x.^(na-1);
    end
    return H
end

### Calculate L_z as 3D array
function Pz(N)
    lim = 15;
    dx = 0.01;

    alpha = 1;
    m = N[2];
    n = N[3];

    x = Array{Float64,2};
    x = -lim:dx:lim;
    am = HermiteCoefficients(m+1);
    am2 = circshift(am, [0 1]);
    an = HermiteCoefficients(n+1);
    F = zeros(m+1,n+1);
    D = zeros(m+1,n+1);
    Fm = Array{Float64,2};
    Fn = zeros((m>n ? m : n)+1, length(x));

    Fmmin1 = Array{Float64,2};
    xFm = Array{Float64,2};
    Fmmin1 = zeros(length(x),1);

    # Calculate Fn
    for nf in 0:length(Fn[:,1])-1
        Hn = Polynomial(an[nf+1,:], x);
        Fn[nf+1,:] = sqrt(alpha)/(2*2^(nf/2)*sqrt(factorial(nf))*pi^(1/4))*exp.(-alpha^2/2*x.^2).*Hn;
    end

    # Calculate Fmn and Dmn
    for mf in 0:m
        xFm = Fn[mf+1,:].*x;
        # Product rule and H'(n) = 2n*H(n-1)
        dFm = mf>0 ? -alpha^2*x.*Fn[mf+1,:] .+ alpha*sqrt(2*mf)*Fn[mf,:]  :  -alpha^2*x.*Fn[mf+1,:];
        
        for nf in 0:n
            F[mf+1,nf+1] = sum(xFm.*Fn[nf+1,:])*dx;
            D[mf+1,nf+1] = sum(dFm.*Fn[nf+1,:])*dx;
            if abs(F[mf+1,nf+1])<1e-6
                F[mf+1,nf+1] = 0;
            end
            if abs(D[mf+1,nf+1])<1e-6
                D[mf+1,nf+1] = 0;
            end
        end
    end
    # To do: use symmetry; use inner-product for F,D


    # Calculate P(m1,n1,m2,n2)
    P = zeros(m+1,n+1,m+1,n+1);
    for m1 in 0:m
        for n1 in 0:n
            for m2 in 0:m
                for n2 in 0:n
                    P[m1+1,n1+1,m2+1,n2+1] = F[m1+1,m2+1]D[n1+1,n2+1] - D[m1+1,m2+1]F[n1+1,n2+1];
                end
            end
        end
    end
    return P
end



### Calculate A operator matrix (from H(t)=e^At H(0) e^-At)
function AA(N)
    P = Pz(N)
    FillOperator((l_,m_,n_,a_,l,m,n,a)-> P[l_+1,m_+1,l+1,m+1]*δ(n_,n)*δ(a_,a)+sigmaz[a_,a]/2*δ(l_,l)*δ(m_,m)*δ(n_,n), N...);
end



### Calculate L_z matrix
function L_z(N)
    P = Pz(N)
    FillOperator((l_,m_,n_,a_,l,m,n,a)-> P[l_+1,m_+1,l+1,m+1]*δ(n_,n)*δ(a_,a), N...);
end



### Calculate x,y,z-spin operator matrices
sig_x(N) = FillOperator((l_,m_,n_,a_,l,m,n,a)-> sigmax[a_,a]*δ(l_,l)*δ(m_,m)*δ(n_,n), N...);
sig_y(N) = FillOperator((l_,m_,n_,a_,l,m,n,a)-> sigmay[a_,a]*δ(l_,l)*δ(m_,m)*δ(n_,n), N...);
sig_z(N) = FillOperator((l_,m_,n_,a_,l,m,n,a)-> sigmaz[a_,a]*δ(l_,l)*δ(m_,m)*δ(n_,n), N...);



### Calculate x,y,z-phases operator matrices
phi_x(N) = FillOperator((l_,m_,n_,a_,l,m,n,a)-> Phi(l_,m_,n_,a_,l,m,n,a,1), N...);
phi_y(N) = FillOperator((l_,m_,n_,a_,l,m,n,a)-> Phi(l_,m_,n_,a_,l,m,n,a,2), N...);
phi_z(N) = FillOperator((l_,m_,n_,a_,l,m,n,a)-> Phi(l_,m_,n_,a_,l,m,n,a,3), N...);
