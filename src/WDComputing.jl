module WDComputing
 
using LinearAlgebra
using Arpack
using SparseArrays
using Printf

include("operators.jl")
include("calc_functions.jl") 
#include("functions.jl") 

export ParametersJ, calcEV, AA, L_z, sig_x, sig_y, sig_z, phi_x, phi_y, phi_z, printmat, CalcRotation, calcSpin, prob, QC_Traj, QC_Traj_param
end # module
