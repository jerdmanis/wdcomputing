
### Print a matrix elegantly
function printmat(mat)
	for j in 1:size(mat)[2]
		print("-------------");
	end
	println();
	for i in 1:size(mat)[1]
		if typeof(mat[1])==Complex{Float64}
			for j in 1:size(mat)[2]
				@printf("%82g   | ", real(mat[i,j]));
			end
			println();
			for j in 1:size(mat)[2]
				@printf("%82gim | ", imag(mat[i,j]));
			end
		else
			for j in 1:size(mat)[2]
				@printf("%82g   | ", mat[i,j]);
			end
		end
		println();
		for j in 1:size(mat)[2]
			print("-------------");
		end
		println();
	end
	println();
end



### Calculate eigenvectors
function calcEV(p::ParametersJ, NEV, N)
# Calculates two lowest eigenvactors and fixes phase

	global FillOperator;
	global H;
	Hm = FillOperator((l_,m_,n_,a_,l,m,n,a)-> abs(l-l_)+abs(m-m_)+abs(n-n_) > 1 ? 0 : H(l_,m_,n_,a_,l,m,n,a,p), N...);
	sol = eigs(Hm, nev=NEV, tol=1e-18, maxiter=100000, which=:SR);
	minus = transpose(sol[2][:,2]);
	plus = transpose(sol[2][:,1]);
	# Keep first component real
	minus *= abs(minus[1])/minus[1];
	plus *= abs(plus[1])/plus[1];
	EE = real(sol[1]);
	
	return minus, plus, EE;
end



### Calculate angle and rotation axis components
function CalcRotation(A)
# Takes A as in U=exp^(-iA) and 
	#globalphase = -(A[1,1]+A[2,2])/2;
	#A = A+[1 0; 0 1]*globalphase;
	alpha = sqrt((A[2,2]-A[1,1])^2+(A[1,2]+A[2,1])^2+(im*A[1,2]-im*A[2,1])^2);
	nz = (A[2,2]-A[1,1])/alpha;
	nx = -(A[1,2]+A[2,1])/alpha;
	ny = (im*A[2,1]-im*A[1,2])/alpha;
	nr = sqrt(nx^2+ny^2);
	
	return alpha, nr, nz;
end



function calcSpin(plus, minus, N)
    # Calculate spin

    sigz = sig_z(N);
    sigy = sig_y(N);
    sigx = sig_x(N);
    
    spin_plus_x = plus*sigx*plus';
    spin_plus_y = plus*sigy*plus';
    spin_plus_z = plus*sigz*plus';
    spin_plus = [spin_plus_x, spin_plus_y, spin_plus_z];

    spin_minus_x = minus*sigx*minus';
    spin_minus_y = minus*sigy*minus';
    spin_minus_z = minus*sigz*minus';
    spin_minus = [spin_minus_x, spin_minus_y, spin_minus_z];

    spin_plusminus_x = plus*sigx*minus';
    spin_plusminus_y = plus*sigy*minus';
    spin_plusminus_z = plus*sigz*minus';
    spin_plusminus = [spin_plusminus_x, spin_plusminus_y, spin_plusminus_z];
    
    return spin_minus, spin_plus, spin_plusminus
end


### Calculate trajectory for Hamiltonian
### Returns an array of ParameterJ values
function makeTrajectory(TR, a, b, offset, Ntr)

	global II, homega;
	if TR==1
	# Trajectory cyclic in x,y-plane
		a = 1;
		b = 1;
		theta = range(0, stop=2*pi, length=Ntr);
		p = Array{ParametersJ}(undef, Ntr);
		for i in 1:Ntr
			phi = [a*cos(theta[i]), b*sin(theta[i]), offset];
			p[i] = ParametersJ(homega,II,(phi,));
		end
	elseif TR==2
	# Trajectory elliptic in y,z-plane
		a = 1;
		b = 01;
		theta = range(0, stop=2*pi, length=Ntr);
		p = Array{ParametersJ}(undef, Ntr);
		for i in 1:Ntr
			phi = [offset a*cos(theta[i]) b*sin(theta[i])];
			p[i] = ParametersJ(homega,II,(phi,));
		end
	end
	
	return p;
end



######## Probability density calculations #########

const HC = HermiteCoefficients(50)

### Calculate nth Hermite polynomial value at x
function H(n,x)
    ### Agrees with function from mathematica until 45th element, needs larger integers
    s = 0
    for k in 1:(n+1)
        s += HC[n+1,k]*x^(k-1)
    end
    return s
end

### Harmonic oscillator eigenfunction #n
psi(n,x,alpha) = 1/sqrt(alpha*factorial(big(n))*2^n)/pi^(1/4)*exp(-(x/alpha)^2/2)*H(n,x/alpha)
#psi(n,x,gc)=1/sqrt( factorial(big(n))*2^n ) *gc^(1/8)/pi^(1/4)*exp(-sqrt(gc)*x^2/2)*H(n,gc^(1/4)*x)


"""
Probability density distribution in momentum ϕ space
"""
### Calculate Probability density as function of phi_z
function prob(x, v, ϕ0, Nx, Ny, Nz)

    #v = solution[2][:,n+1]
    s1 = 0
    s2 = 0

    ### The ones which will be needed to calculate
    psi_up = Array{Complex}(undef,Nz+1)
    psi_down = Array{Complex}(undef,Nz+1)

    for n in 1:(Nz+1)
#        @show 1*factorial(n-1)*2^(n-1)
        psi_up[n] = psi(n-1, x-ϕ0, 1)
        psi_down[n] = psi(n-1, x-ϕ0, 1)
        #psi_up[n] = psi(n-1, x-ϕ0, 1/4)
        #psi_down[n] = psi(n-1, x-ϕ0, 1/4)
    end

    ### A poor mans array reshaping
    c_up = Array{Complex}(undef,Nx+1,Ny+1,Nz+1)
    c_down = Array{Complex}(undef,Nx+1,Ny+1,Nz+1)

    j = 1
    for m in 1:(Nx+1)
        for n in 1:(Ny+1)
            for l in 1:(Nz+1)
                for a in 1:2
                    if a==1
                        c_up[m,n,l] = v[j]
                    elseif a==2
                        c_down[m,n,l] = v[j]
                    end
                    j+=1
                end
            end
        end
    end

    ### might need some improvement
    res = norm(conj(reshape(c_up, (Nx+1)*(Ny+1), (Nz)+1))*psi_up)^2 + norm(conj(reshape(c_down, (Nx+1)*(Ny+1), (Nz)+1))*psi_down)^2
    return res
end





function QC_Traj(;r, theta0, Lz_Lr, phi_min, phi_max, Nphi, multiplier, A_max, N_A)

    ### Integration parameters
    domain = range(phi_min, phi_max, length=Nphi)
    dphi = (phi_max-phi_min)/(Nphi-1)

    ### Form parameters
    # Area may not be too large,
    # because if radius is too large,
    # wave functions are not localised.
    area = range(0.0, A_max, length=N_A)

    ### Basis transformation matrices
    P = [ cos(theta0/2) sin(theta0/2); -sin(theta0/2) cos(theta0/2) ]
    P_inv = [ cos(theta0/2) -sin(theta0/2); sin(theta0/2) cos(theta0/2) ]

    N = length(area)
    alpha = zeros(N)
    delta = zeros(N)
    nz = zeros(N)
    nr = zeros(N)
    for i in 1:N
        theta(phi, A) = asin( r(phi,A)/(Lz_Lr) )

        ### Integrate over a cycle
        I = 0
        for phi in domain
            I += dphi*cos(theta(phi,area[i]))
        end
        I *= multiplier
        delta[i] = I

        ### Evolution operator
        U = [ cos(I/2)-im*cos(theta0)*sin(I/2) im*sin(theta0)*sin(I/2); im*sin(theta0)*sin(I/2) cos(I/2)+im*cos(theta0)*sin(I/2) ]
        int_M = im/2* [ -I 0; 0 I ]
        A = P*int_M*P_inv/(-im)
    
        ### Translate to rotation parameters
        alpha[i], nr[i], nz[i] = CalcRotation(A)
    end

    #### Integrated connection
    #int_M = im/2* [ -I 0; 0 I ]
    #### Evolution operator
    #U2 = P*exp(int_M)*P_inv

    return area, delta

end



function QC_Traj_param(;r, phi, theta0, Lz_Lr, t_min, t_max, Nt, multiplier, A_max, N_A)

    ### Integration parameters
    domain = range(t_min, t_max, length=Nt)
    dt = (t_max-t_min)/(Nt-1)

    ### Form parameters
    # Area may not be too large,
    # because if radius is too large,
    # wave functions are not localised.
    area = range(0.0, A_max, length=N_A)

    ### Basis transformation matrices
    P = [ cos(theta0/2) sin(theta0/2); -sin(theta0/2) cos(theta0/2) ]
    P_inv = [ cos(theta0/2) -sin(theta0/2); sin(theta0/2) cos(theta0/2) ]

    N = length(area)
    alpha = zeros(N)
    delta = zeros(N)
    nz = zeros(N)
    nr = zeros(N)
    for i in 1:N
        theta(phi, A) = asin( r(phi,A)/(Lz_Lr) )

        ### Integrate over a cycle
        I = 0
        for t in domain
            dphi = phi(t+dt,area[i])-phi(t,area[i])
            I += dphi*cos(theta( phi(t,area[1]), area[i]))
        end
        I *= multiplier
        delta[i] = I

        ### Evolution operator
        U = [ cos(I/2)-im*cos(theta0)*sin(I/2) im*sin(theta0)*sin(I/2); im*sin(theta0)*sin(I/2) cos(I/2)+im*cos(theta0)*sin(I/2) ]
        int_M = im/2* [ -I 0; 0 I ]
        A = P*int_M*P_inv/(-im)
    
        ### Translate to rotation parameters
        alpha[i], nr[i], nz[i] = CalcRotation(A)
    end

    #### Integrated connection
    #int_M = im/2* [ -I 0; 0 I ]
    #### Evolution operator
    #U2 = P*exp(int_M)*P_inv

    return area, delta

end
