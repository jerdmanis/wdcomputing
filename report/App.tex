\chapter{Appendix}
\label{ch:App}



\section{Evolution operator}
\label{sc:Con}

The Schr\"{o}dinger equation for the Weyl disk can solved in adiabatic approximation by considering our wave function as the sum of time-dependent eigenfunctions with time-dependent coefficients. 
\begin{equation} \label{eq:EqCoef}
\begin{split}
i\ket{\dot\psi(t)} &= H(t)\ket{\psi(t)} \\
\ket{\psi(t)} &= c_+(t)\ket{+(t)} + c_-(t)\ket{-(t)}.
\end{split}
\end{equation}
Degeneracy is taken into account by saying
\begin{equation}
\begin{split}
	H(t)\ket{+(t)} = 0 \\
	H(t)\ket{-(t)} = 0.
\end{split}
\end{equation}
We are free to choose a reference energy and have set the energies to zero. With this equation \eqref{eq:EqCoef} becomes
$$ \dot c_+(t)\ket{+(t)} + \dot c_-(t)\ket{-(t)} + c_+(t)\ket{\dot+(t)} + c_-(t)\ket{\dot-(t)} = 0. $$
Letting the operators $\bra{\pm(t)}$ work from the left this can be transformed into
\begin{equation} \label{eq:M}
\begin{split}
	\dot {\vec c} &= -M \vec c \\
	\vec c &= \begin{bmatrix}  c_+ \\  c_- \end{bmatrix} \\
	M &= \begin{bmatrix} \braket{+|\dot+} & \braket{+|\dot-} \\ \braket{-|\dot+} & \braket{-|\dot-} \end{bmatrix} .
\end{split}
\end{equation}
This is a very simple differential equation and the solution is
\begin{equation} \label{eq:CoefIE}
	\vec c(t) = e^{-\int{Mdt}}\vec c(0) .
\end{equation}
So we have our evolution operator
\begin{equation}
	U(t) = e^{-\int{Mdt}}
\end{equation}
and recognise the $2\times2$ connection $-M$ for this case.

There is a problem with our formulation above. We have not well defined our states $\ket{+(t)},\ket{-(t)}$, which are (normalised) eigenstates of our Hamiltonian at any time and so are only defined up to a complex phase. What is important for the choice of phase is that the derivatives involved exist and that for the calculation of the evolution over a cycle the initial and final eigenvectors are the same, so the phase of the evolution operator is correct.





\section{A formula for $L_z$}
\label{app:Lz}


We derive here the representation of the operator $\hat{L}_z$ in the harmonic oscillator basis:
\begin{equation}
	Y(x,y) = Y_{mn}\psi_{mn}(x,y) = Y_{mn}F_m(x)F_n(y)
\end{equation}
where $F_m(x)=\frac{\sqrt\alpha}{2\sqrt{2^nn!}\sqrt[^4]\pi} e^{-\alpha^2x^2/2} H_n(\alpha x)$ are harmonic oscillator eigenfunctions and $(Rx,Ry) = (x\cos{\theta}-y\sin{\theta}, y\cos{\theta}+x\sin{\theta})$. Recall that 
\begin{equation}
	i\hat{L}_z=\left. \frac{\partial\tilde U(R)}{\partial\theta}\right|_{\theta=0}
\end{equation}
and
\begin{equation}
	\tilde U(R)Y(x,y) = Y_{mn}F_m(Rx)F_n(Ry)
\end{equation}
Then the matrix elemwnt of $\tilde U$ is
\begin{equation} \label{eq:URmn}
	\tilde U(R)_{m'n',mn} = \langle\psi_{mn}(x,y)|\tilde U(R)|\psi_{m'n'}(x,y)\rangle = \int{F_m(Rx)F_n(Ry)F_{m'}F_{n'}dxdy}
\end{equation}
Now we can extract the matrix elements from this. We take the derivative of \eqref{eq:URmn}
\begin{equation}
	\frac{d\tilde U(R)_{m'n',mn}}{d\theta} = \int \frac{d}{d\theta}\left[F_m(Rx)F_n(Ry)\right]F_{m'}F_{n'}dxdy
\end{equation}
and then calculate derivatives of the harmonics
\begin{equation}
\begin{split}
	\frac{d}{d\theta}\left[F_m(Rx)F_n(Ry)\right] &= \frac{dRx}{d\theta}F_m'(Rx)F_n(Ry)+\frac{dRy}{d\theta}F_m(Rx)F_n'(Ry) \\
	&= (-x\sin{\theta}-y\cos{\theta})F_m'(Rx)F_n(Ry) \\
	&\hspace{15pt} +(-y\sin{\theta}+x\cos{\theta})F_m(Rx)F_n'(Ry) .
\end{split}
\end{equation}
Taking the value at zero angle yields
\begin{equation}
	\left.\frac{d}{d\theta}\right|_{\theta=0} \left[F_m(Rx)F_n(Ry)\right] = -yF_m'(Rx)F_n(Ry) + xF_m(Rx)F_n'(Ry) .
\end{equation}
The end result is 
\begin{equation}
i\hat{L}_{z,m'n'mn} = \int{xF_m(x)F_{m'}(x)dx}\int{F_n'(y)F_{n'}(y)dy} - \int{F_m'(x)F_{m'}(x)dx}\int{yF_n(y)F_{n'}(y)dy} ,
\end{equation}
but we can simplify this into
\begin{equation} \label{eq:FD}
\begin{split}
	i\hat{L}_{z,m'n'mn} &= F_{mm'}D_{nn'}-D_{mm'}F_{nn'} \\
	F_{mn} &= \int xF_m(x)F_n(x)dx \\
	D_{mn} &= \int F_m'(x)F_n(x)dx .
\end{split}
\end{equation} \\

In our case the two levels of interest are close to the same oscillator state $Y(\phi_x,\phi_z) = F(\phi_x-\phi_x^0)F(\phi_z\pm\phi_z^0)$ with different spin and shifted by the reservoir phases. It turns out $\hat L_z$ doesn't couple between these two states. To prove this consider the following two properties of Hermite polynomials:
\begin{equation} \label{eq:HermProp1}
H_n' = 2nH_{n-1}
\end{equation}
and
\begin{equation} \label{eq:HermProp2}
\begin{split}
	xH_n &= \frac{1}{2}H_{n+1}+\frac{1}{2}H_{n}' \\
	&= \frac{1}{2}H_{n+1}+nH_{n-1} .
\end{split}
\end{equation}
This allows $F_{nn},D_{nn}$ to be rewritten to only contain harmonic oscillator functions. Since 
\begin{equation}
	xF_n = \sqrt{\frac{n+1}{2}}F_{n+1} + \sqrt{\frac{n}{2}}F_{n-1} 
\end{equation}
we have
\begin{equation}
	F_{nn} = \int xF_n(x)F_n(x)dx = 0 
\end{equation}
and since 
\begin{equation}
\begin{split}
	F_n' &= -xF_n+\sqrt{2n}F_{n-1} \\
	&= \sqrt{\frac{n}{2}}F_{n-1} - \sqrt{\frac{n+1}{2}}F_{n+1}
\end{split}
\end{equation}
we have also
\begin{equation}
	D_{nn} = \int F_n'(x)F_n(x)dx = 0 .
\end{equation}
The shift in coordinates clearly doesn't change this result, so $\hat L_z$ only couples between "neighbouring" oscillator states. This means that only $\sigma_z$ in $A$ in \eqref{eq:Moore2} contributes to matrix $M$.



\clearpage

\section{Moore's derivation that $U(t)=e^{-iAt}e^{-iBt}$}
\label{sc:Moore}



For Hamiltonians of the special form
\begin{equation} \label{eq:HamMoore}
	H(t) = e^{-iAt} \tilde H e^{iAt} ,
\end{equation}
where $\tilde H$ and $A$ are time independent, it can be shown by direct substitution into the Schr\"{o}dinger equation that the evolution operator is given by
\begin{equation}
	U(t) = e^{-iAt} e^{-iBt} ,
\end{equation}
where $B = \tilde H-A$ \cite{Moore}. If also the Hamiltonian is periodic $H(\tilde t)=H(0)$, the operators $B$ and $e^{-iA\tilde t}$ commute and must have the same eigenstates.
\begin{equation}
\begin{split}
	B\phi_\alpha &= B_\alpha\phi_\alpha \\
	e^{-iA\tilde t}\phi_\alpha &= e^{-i\theta_\alpha}\phi_\alpha	
\end{split}
\end{equation}
This means that these eigenstates are cyclic states of the Hamiltonian, states which return to their initial states after one period.
\begin{equation}
	U(\tilde t) \phi_\alpha = e^{-i\theta_\alpha}e^{-iB_\alpha {\tilde t}}\phi_\alpha
\end{equation}
So the cyclic states get a phase $-\theta_\alpha-B_\alpha\tilde t$ for each cycle, which can be separated into a Berry phase and a dynamical phase. The dynamical part can be evaluated by
\begin{equation}
\begin{split}
	\delta_\alpha &= -\int_{0}^{\tilde t} \braket{\phi_\alpha | U^*(t)H(t)U(t) | \phi_\alpha} dt \\
	&= -B_\alpha \tilde t- \braket{\phi_\alpha | A | \phi_\alpha} \tilde t
\end{split}
\end{equation}
and the geometric part is just the difference
\begin{equation}
	\gamma_\alpha = \braket{\phi_\alpha | A | \phi_\alpha} \tilde t - \theta_\alpha .
\end{equation}





\subsection{Weyl disk system}



The Hamiltonian of our system is of the form \eqref{eq:HamMoore}, so the above results apply. The situation is somewhat simpler, because in our case $e^{-iA\tilde t} = e^{-i.2\pi\sigma_z} = 1$ so $\theta_\alpha = 0$ and because the rotation speed is considered large enough ($\omega \ll \Delta$) such that the dynamical phase is negligible and only the Berry phase $\gamma_\alpha = \braket{\phi_\alpha | A | \phi_\alpha} \tilde t$ remains. This means that in the cyclic basis
\begin{equation}
\begin{split}
	U(\tilde t) &= \begin{bmatrix} e^{i\braket{\phi_+|\sigma_z|\phi_+} \tilde t} & 0 \\ 0 & e^{i\braket{\phi_-|\sigma_z|\phi_-} \tilde t}  \end{bmatrix} \\
	&= e^{i\sigma_z\tilde t} ,
\end{split}
\end{equation}
where we have used that the states do not overlap in coordinate space. This corresponds to $M=-i\omega\sigma_z/2$ in \eqref{eq:U-M}.





\section{Code}



The code for the calculations and numerics is written in Julia and can be executed from https://gitlab.tudelft.nl/jerdmanis/wdcomputing/, where further instructions are given.





%\section{Other Numerics}
%
%
%
%\begin{itemize}
%	\item Other spin directions
%	\begin{center}
%		\includegraphics[width=0.2\textwidth]{../plots/Plot_spin_x(radius).pdf}
%		\includegraphics[width=0.2\textwidth]{../plots/Plot_spin_y(radius).pdf}
%		\includegraphics[width=0.2\textwidth]{../plots/Plot_spin_z(radius).pdf} \\
%		The spins in the x-, y- and z-direction respectively left, in the middle and on the right, as function of the radius on the disc. (What are two lines?)
%	\end{center}
%	\item Average of $\phi$
%	\begin{center}
%		\includegraphics[width=0.2\textwidth]{../plots/Plot_phi_x(radius).pdf}
%		\includegraphics[width=0.2\textwidth]{../plots/Plot_phi_z(radius).pdf} \\
%		The expectation value of the phases in radial direction (left) and z-direction (right) versus the radius on the disc.
%	\end{center}
%	\item Dependence on number of basis functions for $P(\phi_z)$ and $\sigma_z$
%	\item Dependence on the offset.
%\end{itemize}
%
%
%
%
%
%\section{Pertubative solution}
%
%
%We can try and use other states to project our Hamiltonian on and see what discription this gives of the evolution of our system. Another choice of basis states for our system is shifted eigenstates. We use the translation operator to shift harmonic oscillator basis states to the minimum of a quadratic well and use (\ref{eq:M}).
%
%Consider $\psi_0$ to be a harmonic oscillator state, i.e. an eigenstate of the Hamiltonian
%
%\begin{equation} \label{eq:HarmOsc}
%H_{h.o.} = \frac{\vec \phi^2}{2m^*}+V(\vec{\hat{Q}})
%\end{equation}
%We view our Hamiltonian in perturbation theory as
%
%\begin{equation} \label{eq:Pert}
%\begin{split}
%H &= H_0 + H_1 \\
%H_0 &= H_{h.o.} + I\phi_z\sigma_z \\
%H_1 &= I\phi_x\sigma_x + I\phi_y\sigma_y
%\end{split}
%\end{equation}
%Then the following are eigenstates of the unperturbed Hamiltonian $H_0$ (Supp. info, Weyl disk).
%
%\begin{equation} \label{eq:CohStates}
%\ket{\psi_\pm} = e^{i(\vec\phi^T.\vec{\hat{Q}}\mp IL\hat{Q}_z)}\ket{\psi_0}
%\end{equation}
%We take the (shifted) lowest harmonic states for $\sigma_z=\pm1/2$, as our basis and we will refer to them as $\psi_\pm$. Recall from (\ref{eq:M}) the form of our evolution operator
%
%\begin{equation}
%\begin{split}
%	U &= e^{\int Mdt} \\
%	M &= -\begin{bmatrix} \braket{\psi_+|\dot\psi_+} & \bra{\psi_+}\ket{\dot\psi_-} \\ \bra{\psi_-|\dot\psi_+} & \braket{\psi_-|\dot\psi_-} \end{bmatrix}
%\end{split}
%\end{equation}
%The diagonal part of the matrix $M$ doesn't change upon the shift:
%
%\begin{equation}
%\begin{split}
%	\braket{\psi_\pm|\dot\psi_\pm} =& \bra{\psi_0}e^{i(\vec\phi^T.\vec{\hat{Q}}-\sigma_z IL\hat{Q}_z)} \left[i\frac{d\vec\phi}{dt}.\vec{\hat{Q}}e^{-i(\vec\phi^T.\vec{\hat{Q}}-\sigma_z IL\hat{Q}_z)}\ket{\psi_0} + e^{-i(\vec\phi^T.\vec{\hat{Q}}-\sigma_z IL\hat{Q}_z)}\ket{\dot\psi_0} \right] \\
%	=& \bra{\psi_0}e^{i(\vec\phi^T.\vec{\hat{Q}}-\sigma_z IL\hat{Q}_z)}i\frac{d\vec\phi}{dt}.\vec{\hat{Q}}e^{-i(\vec\phi^T.\vec{\hat{Q}}-\sigma_z IL\hat{Q}_z)}\ket{\psi_0} + \\ 
%	&\bra{\psi_0}e^{i(\vec\phi^T.\vec{\hat{Q}}-\sigma_z IL\hat{Q}_z)}e^{-i(\vec\phi^T.\vec{\hat{Q}}-\sigma_z IL\hat{Q}_z)}\ket{\dot\psi_0} \\
%	=& \bra{\psi_0}i\frac{d\vec\phi}{dt}.\vec{\hat{Q}}\ket{\psi_0} + \braket{\psi_0|\dot\psi_0} \\
%	=& \braket{\psi_0|\dot\psi_0} \\
%	=& \;-i\omega \braket{\psi_0|L_z+\sigma_z/2|\psi_0} = \mp i\omega/2,
%\end{split}
%\end{equation}
%since $\hat{Q} = C(\hat{a}^\dagger+\hat{a})$. The components of the $\hat{L}_z$ matrix are $0$ given by (\ref{eq:FD}) and the spins are $\pm1/2$. For the off-diagonal elements the difference in spin gives a contribution
%
%\begin{equation}
%\begin{split}
%	\braket{\psi_\pm|\dot\psi_\mp} &= \braket{\psi_0| e^{\pm i IL\hat{Q}}e^{\pm i IL\hat{Q}} |\dot\psi_0} \\
%	&= \braket{\psi_0| e^{\pm i2 IL\hat{Q}} |\dot\psi_0} = 0 .
%\end{split}
%\end{equation}
%This yields zero because ???\Q{Why?}. The matrix $M$ becomes
%
%\begin{equation}
%	M = \begin{bmatrix} -i\omega/2 & 0 \\ 0 & i\omega/2 \end{bmatrix} .
%\end{equation}
%
