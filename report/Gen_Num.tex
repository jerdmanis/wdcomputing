\chapter{Numerical evaluation of the connection}

\label{ch:Gen_Num}

Here we give some numerical results corresponding to the previous chapter characterising circular trajectories of the isotropic system in the quasi-classical regime. For convenience we do all calculations in the cyclic basis where one basis state is in the upper minimum and one in the lower minimum and from here on the states $\ket{+}$ and $\ket{-}$ respectively will denote these states. A very small offset is used ($\phi^r_z=0.001$) for the calculations on the qubit to ever so slightly break degeneracy, so it is possible to get the wanted cyclic eigenstates easily by means of an eigensolver. We approximate the infinite dimensional Hilbert space by harmonic oscillator functions for each dimension.

We make sure we are in the correct limit by reproducing the energy spectrum of the Weyl disk paper \cite{WP2}. To see whether the states are localised in superconducting phase space due to quasi-classics, we plot the wavefunctions. Finally we present the most important result, the geometric phase for different radii and the $\sigma_z$-operator responsible for this phase, and test this for convergence.





\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.45\textwidth]{../plots/Plot_E(radius).pdf}
		\includegraphics[width=0.45\textwidth]{../plots/Plot_E(offset).pdf}
		\caption{The first four energy levels of \eqref{eq:Ham} as function of the radius of the circular trajectory on the disc (\emph{left}) and offset of the circular trajectory from the disc (\emph{right}) in phase space. The degeneracy of the eigenenergies which form the Weyl disk is reproduced. The right plot also shows that the levels can be split for measurement and initialisation. (The number of harmonic basis functions used is $20^3$. Parameters are $L_z=4, L_r=4/3, C_n=20, I_n=1$.) }
		\label{fig:E}
	\end{center}
\end{figure}

\clearpage

\section{Numerical method}



An eigensolver is used on the Hamiltonian in harmonic oscillator basis to calculate the eigenenergies, the probability density functions and the rotation on the Bloch sphere. This is done for different values of the reservoir phases. To examine the rotation on the Bloch sphere the radius on the disc is varied and then a rotation of the reservoir phases is simulated using the end result of the previous chapter, equation \eqref{eq:U_diabatic_limit}. The parameters for the Hamiltonian \eqref{eq:Ham} are chosen $L_z = 4, L_r = 4/3, I_n = 1, C_n=20$. Note that we do not have to constrict the range of the phases $\phi$ as we can scale them with the other parameters ($I_n,L_n,C_n$) to obtain the same results for smaller $\phi$.
For this reason the $I_n$ can also be taken unity. The values for $I_n$ create an isotropic disc and make the easy direction $n=z$. The capacities are taken large to be deep in the quasi-classical limit, however, as we will see later, convergence poses a restriction on this. In figure \ref{fig:E} the Weyl disk (shown in figure \ref{fig:WD}) is reproduced using these values. It shows the degeneracy remains for increasing radius and is lifted by creating an offset. This is needed so the levels remain degenerate and can potentially be initialised or read out by adding an offset.





\subsection{Representing the connection}
\label{pr:SO3}



The evolution operator for the two lowest levels is calculated for the chosen parameters. It is verified to be unitary, which confirms our analysis. To investigate the unitary evolution operator as rotation on the Bloch sphere, notice that any unitary operator can be written $U = e^{i\gamma} e^{-i\alpha/2.\vec{\sigma} .\hat{n}} = e^{-i\alpha/2.\vec{\sigma}.\hat{n} + \gamma.1}$. Equations for the parameters in terms of our matrix elements are

\begin{equation} \label{eq:Unitary}
	U = e^{iA\tilde t} = \exp\left(i\begin{bmatrix} a_{11} & a_{12} \\ a_{21} & a_{22} \end{bmatrix}\right) = \exp\left(i\begin{bmatrix} -\alpha/2.n_z+\gamma & -\alpha/2.(n_x-in_y) \\ -\alpha/2.(n_x+in_y) & +\alpha/2.n_z+\gamma \end{bmatrix}\right) ,
\end{equation}

where $a_{ij}$ are the elements of $A\tilde t$. We want to extract the angle $\alpha$ from our matrix $A$.
%We calculate the global phase factor $\gamma$\Q{Is this relevant?} simply with
%\begin{equation}
%	\gamma = (a_{11}+a_{22})/2.
%\end{equation}
For the rotation axis direction $\hat{n}$ and rotation angle $\alpha$ the system of equations is
\begin{equation}
	a_{12}+a_{21} = -\alpha n_x,   \hspace{15pt}   ia_{21}-ia_{12}=\alpha n_y ,   \hspace{15pt}   a_{22}-a_{11}=\alpha n_z .
\end{equation}
This is complemented with
\begin{equation}
	\alpha = \sqrt{(\alpha n_x)^2+(\alpha n_y)^2+(\alpha n_z)^2}.
\end{equation}
Then $\alpha$ is determined by
\begin{equation}
	\alpha = \sqrt{(a_{12}+a_{21})^2+(ia_{21}-ia_{12})^2+(a_{22}-a_{11})^2} .
\end{equation}
%and then calculating $\hat n$ and the rotation axis angle is trivial.





\clearpage
\section{Results}





\subsection{The ground states}



Figure \ref{fig:overlap} shows the two lowest states in the plane of the disc for different distances from the centre. They are localised and separated states. The further away from the centre of the disc, the more the wavefunctions of the lowest states move towards each other, and the weaker the localisation becomes. At some point the wave functions start to overlap. The plots are interpreted as indicating a double-well potential, meaning two wells separated by a barrier. The barrier lowers by moving away from the centre, which makes the wavefunctions approach each other. If the minima are close enough, an even and odd state emerge. Eventually the minima seem to join together (as we will see later in the quasi-calassical approach in figure \ref{fig:Potential}), because a ground and excited harmonic state can be recognised. These states could be utilised to initialise and read out the qubit or form the basis of the qubit.

\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_Prob(phi_z)_0.pdf}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_Prob(phi_z)_2.pdf}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_Prob(phi_z)_2,2.pdf}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_Prob(phi_z)_2,6.pdf}
		\caption{The probability function $P(\phi_z)$ for the two lowest levels of \eqref{eq:Ham}. \emph{Top left}: $\phi^r_x = 0$, \emph{Top left}: $\phi^r_x = 2$, \emph{Top left}: $\phi^r_x = 2.2$, \emph{Top left}: $\phi^r_x = 2.6$. The $\phi^r_y$ is always takn to be zero. The wavefunctions are quite localised, but move towards each other and become broader when moving away from the disc centre. They first form an even and odd state and later two harmonic states. (The number of harmonic basis functions used is $20^3$. Parameters are $L_z=4, L_r=4/3, C_n=20, I_n=1$.) 
			%However, they do not overlap at least until $\phi^r_x = 8$.
		}
		\label{fig:overlap}
	\end{center}
\end{figure}





\subsection{Connection}



The calculations show our chosen states are cyclic and only obtain a geometric phase without occupation changes. In figure \ref{fig:RotCirc} the geometric phase and in figure \ref{fig:CirclesNs} the quasi-spin in the $z$-direction are displayed. The geometric phase follows precisely the quasi-spin of the two levels, as expected from equation \eqref{eq:M_adiabatic_appr}. Our hypothesis that our qubit states are harmonic oscillator ground states, so the $L_z$-operator doesn't couple them, seems right. Although there seem to be no non-diagonal elements in the connection, fortunately the angle over which is rotated increases with radius. This is due to the spin following (the direction of) the parameter phases (which will be tested also in the quasi-classical approximations in \ref{fig:SpinPhi}). This allows for rotation over the Bloch sphere around the $z$-axis for these cyclic states.


To show that we have a good approximation of our eigenfunctions and Hilbert space, we have varied the number of harmonic basis functions in figure \ref{fig:CirclesNs}. We have done this for several capacities and show the result for two values. We see that only for small capacities (up to $C=20$) we have good convergence. For larger values convergence is unsatisfactory, which is why the value $C=20$ was chosen for all curves.


\begin{figure}[h!]
	\begin{center}
		%		\includegraphics[width=0.4\textwidth]{../plots/Plot_spin_z(radius).pdf}
		\includegraphics[width=0.5\textwidth]{../plots/Plot_alpha(radius).pdf}
		%		\includegraphics[width=0.3\textwidth]{../plots/Plot_n(radius).pdf}
		\caption{Plot showing the angle of rotation on the Bloch Sphere for traversing one cycle (\emph{middle} and \emph{right} respectively), all as function of the radius (of the circular trajectory). The Hamiltonian is \eqref{eq:Ham}. By varying the radius and possibly traversing multiple cycles any phase could be acquired. (The number of harmonic basis functions used is $20^3$. Parameters are $L_z=4, L_r=4/3, C_n=20, I_n=1$.)}
		\label{fig:RotCirc}
	\end{center}
\end{figure}


\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.45\textwidth]{../plots/Plot_spins_Ns_5.pdf}
		\includegraphics[width=0.45\textwidth]{../plots/Plot_spins_Ns_20.pdf}
		\caption{The expectation value of the $\sigma_z$-operator (\emph{top} lines) and off-fiagonal terms (\emph{bottom} lines). The number of basis states is varied for $C_n=20$ and $C_n=5$. There is convergence for $C_n=5$ and $C_n=20$, but for larger values it will be too slow. We see that spins of both levels are aligning with the disc as we move away from the centre. (Other parameters: $L_z=4,L_r=4/3, I_n=1$.) }
		\label{fig:CirclesNs}
	\end{center}
\end{figure}


