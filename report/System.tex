\chapter{System}
\label{ch:System}



\section{Introduction to Weyl Disks}
\label{sc:ItWD}



\subsection{Multiterminal Josephson junction}



It has recently been shown that multiterminal superconducting junctions (figure \ref{fig:MTJJ}) with topologically trivial leads and without exotic materials can be tuned such that two levels cross at zero energy (relative to Fermi level) in the ABS spectrum and that they can be brought to a \emph{Weyl point} or Weyl singularity \cite{WP1}. This is a point in momentum space (in our case the superconducting phase $\phi$ takes the role of momentum) where two bands cross linearly (degeneracy). What is special about this point, is that it has topological protection, meaning in our case that any degeneracy is not easily lifted. The tuning parameters are the superconducting phases of the terminals, but due to gauge invariance one phase can be set to zero so the others span the parameter space.

Additionally, such Weyl point can be extended in two (or three) directions by coupling to external circuits, creating a \emph{Weyl disks} \cite{WP2}. The benefit of this all is a region, in which it is possible to move around without lifting degeneracy and even without fluctuations, making coherence time long.
Our system is such a four-terminal Josephson junction with a nanostructure in the centre which exhibits a Weyl disk with nearly degenerate levels. It is mentioned at the end of the paper \cite{WP2} that the Weyl disk system is interesting because of the possibilities for quantum computation. We intend to investigate if this system could establish a qubit.

\begin{figure}[!b]
\begin{center}
	\includegraphics[width=0.9\textwidth]{figures/MTJJ.png} \\
	\caption{\textit{Left}: Multiterminal superconducting junction with superconducting phases $\phi_n$. \textit{Middle}: The spectrum as function of $\phi_1$ with the other reservoir phases away from the Weyl singularity. $\Delta$ stands for the superconducting energy gap. \textit{Right}: The spectrum tuned by means of reservoir phases to the point of an energy level crossing. \cite{WP1}}
	\label{fig:MTJJ}
\end{center}
\end{figure}





\subsection{Four-terminal Josephson junction with soft constraint}
	


We should look first at the Hamiltonian to understand the dynamics and, in order to find it, the behaviour of the phase parameters must first be explained. They aren't fixed numbers (hard constraint) in a realistic setup, but are susceptible to backaction from the system and fluctuations and are therefore dynamical quantum variables \cite{WP2}. This constitutes a \emph{soft constraint}. To model this scenario an energy term of the form $(\hat\phi-\phi^r)^2/2L$, where $\phi$ is a superconductor phase and $L$ is a inductance, is added to the Hamiltonian pulling the operator $\hat\phi$ towards the value $\phi^r$, together with a charge term $\hat{Q}^2/2C$, where $\hat Q$ is the charge operator and $C$ a capacitance, accounting for the fluctuations of $\hat\phi$.

The system is embedded in a linear circuit to account for the adjusted dynamics just discussed. Naturally the inductivity provides a soft constraint and the capacitances provide fluctuations (figure \ref{fig:WDSetup}). The phases in the nanostructure are softly constrained to the phases of the leads, determined by the superconducting loops used to set the phase differences between the leads. When this is done, the Weyl point (in the phase space of the nanostructure) spreads out over a two-dimensional disc in the space of reservoir phases $\phi^r_n$ (figure \ref{fig:WD}).

\begin{figure}
\begin{center}
	\includegraphics[width=0.45\textwidth]{figures/Weyl_disc_setup.png} \\
	\caption{Model of the four-terminal Josephson junction in a realistic environment. The contacts have certain capacity and the superconducting leads have inductances, which are modeled in the circuit. \cite{WP2}}
	\label{fig:WDSetup}
\end{center}
\end{figure}
	

\begin{figure}
\begin{center}
	\includegraphics[width=0.45\textwidth]{figures/Weyl_disc.png} \\
	\caption{\textit{Left}: Depiction of the Weyl disk system for \eqref{eq:Ham0}. The surface is where three minima of the nanostructure exist. The red disc is where two minima become degenerate and is called the Weyl disk. (Parameters: $L_n=L/n, I_n=I$.) \textit{Right}: The energy spectrum for varying the reservoir phase within the disc (above) and out of the disc (below). The reservoir phases are scaled with $\phi_0$. \cite{WP2}}
	\label{fig:WD}
\end{center}
\end{figure}


Now we can build the Hamiltonian. Around the Weyl point it is effectively $H_{WP} =(\hbar/2e) \sum_{n=x,y,z} I_{n} \hat\phi_n \hat\sigma_n$, where $\hat\sigma_n$ are the Pauli matrices in the space of ground and excited singlet states, $I_n$ are the coefficients of the linear crossing and $\hat\phi_n$ is the superconducting phase relative to the Weyl point. The full Hamiltonian also considering the soft constraint is 
\begin{equation} \label{eq:Ham0}
	H(\vec{\phi^r}) = \frac{\hbar}{2e}\sum_{n=x,y,z} I_n\hat{\phi}_n\sigma_n + \left(\frac{\hbar}{2e}\right)^2 \frac{(\hat{\phi}_n-\phi_n^r)^2}{2L_n} + \frac{\hat{Q_n}^2}{2C_n} ,
\end{equation}
where the middle term accounts for the soft constraint and the last term for the fluctuations. The system can be better understood when one considers only one dimension. Then the Hamiltonian and quasi-spin can be diagonalised simultaneously so that we can write the kinetic part ("$\phi$-part") as $(\hat\phi-\phi^r+\sigma IL)^2/2L-I^2L/2$. Classically there are two degenerate minima at $\phi^r-\sigma IL$ for the two quasi-spin values, separated by a barrier of height $E_B=LI^2/2$.

However, for this situation there exist two regimes. To distinguish between regimes, a quasi-classical parameter is defined
\begin{equation} \label{eq:QCP}
Q = \frac{1}{2} \left(\frac{LIe}{\hbar}\right)^2\frac{\hbar}{e^2Z},
\end{equation}
where $Z=\sqrt{L/C}$ is the characteristic impedance of the oscillator. This is the ratio of the barrier height to the oscillator energy quantum. If $Q\gg1$, the states in the two minima don't notice each other and the system is in the quasi-classical regime. The states can be considered localised in this case and can be approximated by a harmonic oscillator for each quasi-spin value. For $Q\ll1$ the opposite is true, and the coupling between the states makes quantum effects become stronger. 

In the 3D case something similar happens. The minima are separated in the direction of largest $I_n^2L_n$, which is called the easy-direction and corresponds to $n=1$ in figure \ref{fig:WD}. The Weyl disk system will be considered in the quasi-classical regime, where the influence of soft constraint is well noticeable. Then we have a Weyl disk in the plane in directions $n=2,3$.
It must be noted that there are two 3D spaces of the superconductor phases. One is the parameter space, the space of the reservoir phases $\phi^r_n$. This is where the Weyl point is spread out over a disc. The other is the coordinate space, the space of coordinates $\phi_n$ corresponding to the operators $\hat\phi_n$, where the minima of the double-well potential are.



\clearpage

\subsection{Weyl disk computation}



Considering the possibility to make loops in the space of reservoir phases without breaking degeneracy of the ground states, it is interesting to see whether the Weyl disk system can be used for holonomic computation. It is possible to take advantage of the two lowest levels residing in the minima in coordinate space (space of $\phi_n$) for qubit operation. The quasi-classical regime is needed, so the states do not couple too strongly and don't show unwanted interaction and perhaps become non-degenerate. We will namely consider only the adiabatic limit. The parameters in the Hamiltonian will be the external phases $\phi^r_n$. By traversing a loop in the Weyl disk it is hoped that relative phase factors arise (rotation around z-axis of Bloch sphere). Then these could then be converted into arbitrary unitary operations (rotations) by measuring in a different basis. That might open the window to a variety of quantum gates.

There is the problem of reading out and setting up the state of the qubit. This could be done by moving to the outside of the disc, so the states will come together, couple and split or by moving in the easy direction off the disc  and split the eigenspace into two levels each residing in one of the minima. Then probing or initialising might be done in a way similar to that in the previously discussed Andreev qubit system, coupling the levels to a resonator \cite{ALQ3}.

We may expect desirable behaviour from our qubit. Noise should play a much smaller role here than for the Andreev quantum dot and other systems. The holonomic/geometric nature of the manipulation and the topological protection of the Weyl point/disc account for this. By virtue of the degeneracy of the two qubit states, the dynamical dephasing is expected to be minimal, certainly since the degeneracy is topologically protected. The dynamical phase should be the same for both, and this would not alter the qubit state. The relaxation time depends on the separation in the parameter space. If the wave functions overlap, the quasi-particles spontaneously migrate from one state to the other. This means no difficulty in our case during manipulation, as we restrict ourselves to a degenerate level which excludes overlap since that would cause an energy splitting.

To verify our expectations, the behaviour of the qubit around the Weyl disk is assessed by both analytical and numerical results. The results presented here are valid in the regime $\Delta \ll \omega \ll \omega_0$, where $\Delta$ is the energy difference between the almost degenerate levels and $\omega_0$ is the energy quantum in the spectrum of the coupled harmonic oscillator (as mentioned in the 1D example above) and $\omega$ is the frequency of the cycle through parameter space. This constraint guarantees that the levels interact with each other, but not with any higher levels thanks to adiabaticity.



%!!! From the previous section it can be concluded that the Hamiltonian is tunable by the reservoir phases. The question now is, whether this enables us to go from one state to the other within a degenerate subspace. An answer can be given in the holonomic computation framework, but we restrict discussion to periodic trajectories and their associated geometric phase. \towrite