\chapter{Trajectories}
\label{ch:QC_Num}




Now that we have the quasi-classical connection, we can do some calculations with them for some simple trajectories. We have chosen the circle, which we already studied in chapters \ref{ch:Gen_Ana} and \ref{ch:Gen_Num}, a circle with the disc centre outside, on its edge and inside, an ellipse, and a square. We first give an analytical treatment of these examples and compare the results. Then we calculate the connections.





\section{Parametrisation}



To perform the integral of \eqref{eq:U-M} it is needed to express the integrand $\cos\theta$ in $\varphi$. We make explicit the dependence on $r$, displayed in figure \ref{fig:spinCond}, and then $r(\varphi)$ is given per trajectory. The angle $\theta$ is determined by
\begin{equation}
	\tan\theta
	= \frac{\phi_r^0}{\phi_z^0} = \frac{r} {\sqrt{(L_z-L_r)^2 - r^2}}
	= \left[\left(\frac{L_z-L_r}{r}\right)^2 - 1\right]^{-1/2} .
\end{equation}
By using the relation
\begin{equation}
\cos(\arctan x) = \frac{1}{\sqrt{(x^2+1)}} 
\end{equation}
the integrand can be written as function of the parameter $r$
\begin{equation}
	\cos\theta
	= \sqrt{1-\frac{r^2}{(L_z-L_r)^2}} .
\end{equation}
We set $\rho=\frac{r}{L_z-L_r}$. We also consider a Taylor expansion of this formula, which is valid for small $\rho$:
\begin{equation} \label{eq:apprCos}
	\cos\theta = \sqrt{1-\rho^2} \approx 1-\rho^2/2
\end{equation}
If $r$ can be written as a function of $\varphi$, which requires $\varphi(t)$ to be one-to-one, then
\begin{equation} \label{eq:M_int}
	\int_0^{\tilde\tau} M_\varphi \frac{d\varphi}{dt} dt
	= \int_0^{2\pi} M_\varphi d\varphi
%	= \frac{i\omega}{2} \begin{bmatrix} -\cos{\theta} & \sin{\theta} \\ \sin{\theta} & \cos{\theta} \end{bmatrix} .
\end{equation}
With these considerations made, we are ready to evaluate the connection for specific trajectories.






\section{Circle centred}



A circle is straightforward to analyse here, since the radius and thus integrand don't change. We say
\begin{equation}
	\rho(\varphi)=R % , \qquad \varphi(t)=\omega t
\end{equation}
and so components of the integral in \eqref{eq:M_int} can be evaluated:
\begin{equation} \label{eq:Circle}
	\int_0^{2\pi} \cos\theta d\varphi = 2\pi \cos\theta = 2\pi \sqrt{ 1-R^2 } .
\end{equation}





\section{Circle not enclosing centre}



Now we would like to know what happens when the trajectory is not in the middle of the disc. We take a circle just next to the origin to compare, because this has an elegant form, but this also turns out to give an interesting result. The describing equation is
% See for derivation: https://ocw.mit.edu/courses/mathematics/18-01sc-single-variable-calculus-fall-2010/unit-4-techniques-of-integration/part-c-parametric-equations-and-polar-coordinates/session-82-polar-coordinates/MIT18_01SCF10_Ses82d.pdf
\begin{equation}
	\rho(\varphi)=2R\cos\varphi ,
\end{equation}
where $\varphi=-\pi/2...\pi/2$. This gives the integral

\begin{equation}
	\delta = \int_{-\pi/2}^{\pi/2} \cos\theta d\varphi
	= 2\int_0^{\pi/2} \cos\theta d\varphi
	= 2\int_0^{\pi/2} \sqrt{ 1-4R^2\cos^2\varphi } d\varphi ,
\end{equation}
where the symmetry of $\cos(\varphi)$ was used. We rewrite as an elliptic integral of the second kind, which is given by $E(\varphi|k^2)=\int_0^{\varphi} \sqrt{ 1 +k^2\sin^2x}\; dx$:
\begin{equation} \label{eq:OffCircle}
	\delta = 2\sqrt{1-4R^2} \int_0^{\pi/2} \sqrt{ 1 +\frac{4R^2}{1-4R^2}\sin^2\varphi } d\varphi
	= 2\sqrt{1-4R^2} E\left(\sqrt{\frac{4R^2}{1-4R^2}}\right) ,
\end{equation}
where $E(k)=E(\pi/2|k^2)$ is the complete elliptic integral of the second kind. 

If we use our approximation \eqref{eq:apprCos} the integral reads
\begin{equation}
	\delta \approx 2\int_0^{\pi/2} \cos\theta d\varphi
	= 2\int_0^{\pi/2} 1-2R^2\cos^2\varphi d\varphi = \pi(1-R^2).
\end{equation}
This shows an interesting difference compared to the regular circle, namely that the angle for vanishing radius is $\pi$ which is twice as small. This is not surprising as the range of $\varphi$ and therefore of the integral is also only half a round here. So it is clear that the rotation angle in parameter space is very significant for the rotation angle on the Bloch sphere.





\section{Ellipse}



The polar equation of an ellipse around the origin is
\begin{equation}
	\rho(\varphi)=\frac{b}{\sqrt{1-(e\cos\varphi)^2}} , %, \qquad \varphi(t)=\omega t .
\end{equation}
where $e=\sqrt{1-(b/a)^2}$ is the eccentricity. The integral \eqref{eq:M_int} in this case is
\begin{equation}
	\delta = \int_0^{2\pi} \cos\theta d\varphi
	= \int_0^{2\pi} \sqrt{ 1-\frac{b^2}{1-(e\cos\varphi)^2} } d\varphi .
\end{equation}
We further rewrite making the substitution $x=e\cos\varphi$ to change variable convert into another possible form
\begin{equation} \label{eq:Ellipse}
	\delta = 4\int_0^{\pi/2} \sqrt{ \frac{1-b^2-(e\cos\varphi)^2}{1-(e\cos\varphi)^2} } d\varphi
	= -\frac{4}{e} \int_0^1 \sqrt{ \frac{1-b^2-x^2} {(1-e^{-2}x^2)(1-x^2)} } dx ,
\end{equation}
where we have used that the integrand is periodic with period $\pi$ and is symmetric around $\phi=\pi/2$ to divide the domain of integration into 4 equal parts.

This seems not to be solvable algebraically in general, but one might consider special cases where simultaneously $b=0 \land e=1$, or $b=1$ or $e=1 \land b\neq0$. The first holds in the limit of very small semi-minor axis $b$ compared to $L_z-L_r$  and semi-major axis $a$ and simplifies the first integral in \eqref{eq:Ellipse} to $-2\pi$. For the second situation $b=1$ we get
%\begin{equation}
%	-\frac{4}{e} \int_0^1 \sqrt{ \frac{-1}{(1-e^{-2}x^2)(1-x^2)} } xdx
%	= -\frac{2}{e} \int_0^1 \sqrt{ \frac{-1}{(1-e^{-2}y)(1-y)} } dy .
%\end{equation}
\begin{equation}
	\delta = 4e\int_0^{\pi/2} \frac{1}{\sqrt{ 1-(e\cos\varphi)^2 }} \cos\varphi d\varphi
	= \frac{4e}{\sqrt{1-e^2}}\int_0^{\pi/2} \frac{\cos\varphi}{\sqrt{ 1+\frac{e^2}{1-e^2}\sin^2\varphi }} d\varphi .
\end{equation}
This is similar to an elliptic integral of the second kind and a solution exists given by
\begin{equation}
	\delta = 4 \left[ \arcsin\left(\frac{e}{\sqrt{1-e^2}} \sin x \right) \right]_0^{\pi/2}
	= 4 \arcsin\left(\frac{e}{\sqrt{1-e^2}}\right) .
%	= 4e\int_0^1 \sqrt{ \frac{1}{1-e^2+e^2z^2} } dz
\end{equation}
Finally, $e=1$ corresponds to a very large semi-major axis $a$ and is perhaps not very realistic, also if this assumption is made, then the result is
\begin{equation}
\begin{split}
	\delta &= 4\int_0^{\pi/2} \sqrt{ \frac{\sin^2\varphi-b^2}{\sin^2\varphi} } d\varphi
	= 4\int_0^{\pi/2} \sqrt{ 1-\frac{b^2}{\sin^2\varphi} } d\varphi
	= 4bi \int_0^{\pi/2} \frac{\sqrt{ 1-b^{-2}\sin^2\varphi }}{\sin\varphi} d\varphi .
%	&= 4bi \left[ 
%	-1/2 \ln \frac{ \sqrt{1-\frac{1}{b^2}\sin^2\varphi} + \cos\phi }
%	{ \sqrt{1-\frac{1}{b^2}\sin^2\varphi} - \cos(\phi) }
%	+ b\ln\left( \frac{1}{b} \cos\varphi + \sqrt{1-\frac{1}{b^2}\sin^2\varphi} \right) \right]_0^{\pi/2}
\end{split}
\end{equation}
%\begin{equation}
%	-\frac{4}{e} \int_0^1 \sqrt{ \frac{1-b^2-x^2} {(1-x^2)(1-x^2)} } dx
%	= -\frac{4}{e} \int_0^1 \frac{\sqrt{ 1-b^2-x^2 }}{1-x^2}  dx
%	= -\frac{4}{e} \int_0^1 \sqrt{ \frac{b^2}{(1-x^2)^2} -1 } dx .
%\end{equation}
This integral diverges similarly to $\int dx/x$, as one might expect for large semi-major axis, which is not very useful for our purpose.
% int sqrt( A^2-B^2/(1-e^2*cos^2(x)) ) from 0 to 2*pi
% int 1/sqrt(1-e^2*cos^2(x)) from 0 to 2*pi

%\begin{equation}
%	r = (L_z-L_r) (\tan^{-2}\theta + 1)^{-1/2} .
%\end{equation}
The Taylor series approximation \eqref{eq:apprCos} simplifies the general integral considerably and results in
\begin{equation}
\begin{split}
	\delta &\approx \int_0^{2\pi} 1-\frac{b^2}{2-2(e\cos\varphi)^2} d\varphi = 2\pi- \frac{b^2}{2} \int_0^{2\pi} \frac{1}{1-e^2\cos^2\varphi} d\varphi \\
	&= 2\pi - 4\cdot \frac{b^2}{2\sqrt{1-e^2}} \arctan \left.\left( \frac{\tan(\varphi)} {\sqrt{1-e^2}} \right)\right|_0^{\pi/2} =  2\pi\left(1-\frac{b^2}{2\sqrt{1-e^2}}\right).
\end{split}
\end{equation}





\section{Square}



The parametrisation of one side of the square is 
\begin{equation}
	x(t)=R, \qquad y(t)=Rt
\end{equation}
for $t=-1\dots1$, or in polar form
\begin{equation}
	\rho(t)=R\sqrt{1+t^2}, \qquad \tan\varphi(t)= t
\end{equation}
in the same time domain. We express again $\rho$ in $\varphi$:
\begin{equation}
	\rho(\varphi) = R\sqrt{1+\tan^2\varphi} = \frac{R}{|\cos\varphi|} ,
\end{equation}
where $\varphi=-\pi/4\dots\pi/4$. We can consider only this quarter and multiply by four by symmetry. This gives the integral

\begin{equation}
	\delta = 4\int_{-\pi/4}^{\pi/4} \cos\theta d\varphi
	= 4\int_{-\pi/4}^{\pi/4} \sqrt{ 1-\frac{R^2}{\cos^2\varphi} } d\varphi .
\end{equation}
We convert our expression to something similar to an elliptic integral of the second kind
\begin{equation}
	\delta = 4\int_{-\pi/4}^{\pi/4} \frac{1}{\cos\varphi} \sqrt{ 1-R^2-\sin^2\varphi } d\varphi
	= 4\sqrt{1-R^2} \int_{-\pi/4}^{\pi/4} \frac{1}{\cos\varphi} \sqrt{ 1-\frac{1}{1-R^2} \sin^2\varphi } d\varphi .
\end{equation}
This integral has been solved and the solution is
\begin{equation} \label{eq:Square}
\begin{split}
	\delta &= \left. 2R
	\ln \frac{ \sqrt{ 1-\frac{1}{1-R^2} \sin^2\varphi } + \sqrt{\frac{R^2}{1-R^2}} \sin(\phi) }
	{ \sqrt{ 1-\frac{1}{1-R^2} \sin^2\varphi } - \sqrt{\frac{R^2}{1-R^2}} \sin(\phi) }
	+ 4\arcsin\left(\sqrt{\frac{1}{1-R^2}} \sin\varphi\right) \right|_{-\pi/4}^{\pi/4} \\
%	&= \left. 2R
%	\ln \frac{ \sqrt{ 1-R^2-\sin^2\varphi } + R \sin(\phi) }
%	{ \sqrt{ 1-R^2-\sin^2\varphi } - R\sin(\phi) }
%	+ 4\arcsin\left(\sqrt{\frac{1}{1-R^2}} \sin\varphi\right) \right|_{-\pi/4}^{\pi/4} \\
	&= 4R
	\ln \frac{ \sqrt{\frac{1}{R^2}-2} + 1 }
	{ \sqrt{ \frac{1}{R^2}-2 } - 1 }
	+ 4\arcsin\left(\frac{1}{\sqrt{2-2R^2}}\right) .
\end{split}
\end{equation}
%We may also do
%\begin{equation}
%	\cos\theta\sin\theta
%	= \frac{1}{2} \sin 2\theta
%	= \frac{1}{2} \cos [2(\theta-\pi/8)]
%	= \cos^2 (\theta-\pi/8) - \frac{1}{2}
%\end{equation}
%\begin{equation}
%	\int_{-\pi/4}^{\pi/4} \cos\theta\sin\theta d\varphi
%	= \int_{-\pi/4}^{\pi/4} \cos^2 (\theta-\pi/8) - \frac{1}{2} d\varphi
%	= \int_{-3\pi/8}^{\pi/8} \cos^2 \theta d\varphi -\pi/4
%\end{equation}

Approximating again the integrand using \eqref{eq:apprCos} greatly simplifies the expression. We have
\begin{equation}
	\delta \approx 4\int_{-\pi/4}^{\pi/4} \cos\theta d\varphi
	= 4\int_{-\pi/4}^{\pi/4} 1-\frac{R^2}{2\cos^2\varphi} d\varphi = 2\pi - 2R^2 \left[\tan\varphi\right]_{-\pi/4}^{\pi/4} = 2\pi-4R^2 .
\end{equation}
The result is comparable to that of the circle in \eqref{eq:Circle}.






\section{Comparison}



We see in figure \ref{fig:CirclesCapacities} that our quasi-classical approximations seem valid and the calculated connection for the circle closely resembles the numerical result of chapter \ref{ch:Gen_Num} in the limit of large capacity. Capacities larger than $C=20$ are not considered, because convergence is not guaranteed. The slope, particularly for small radius, is almost the same. A difference is the value at zero area present for the numerics. This is probably not caused by the number of basis states used, since figure \ref{fig:CirclesNs} does not converge to zero at the beginning, but it might be explained by the fact that a very small offset from the disc was used. For large radius the quasi-classical line diverges, however, for these values the wavefunctions would overlap and the approximations no longer hold.


\begin{figure}[h!]
	\begin{center}
		\includegraphics[width=0.55\textwidth]{../plots/Plot_circle_capacities.pdf}
		\caption{The geometric phase for traversing one circle as function of the enclosed area for exact treatment and for the quasi-classical approach. The lines with capacities are of course from the exact formulas. The results in quasi-classical approximations show very good agreement with numerics for small area. The slope is correct, but there is a difference in offset, perhaps as a result of the small offset in the earlier numerics. (The number of harmonic basis functions used is $20^3$. Parameters: $L_z=4,L_r=4/3, I_n=1$.)}
		\label{fig:CirclesCapacities}
	\end{center}
\end{figure}

\clearpage


For each of the trajectories geometric phase has been calculated as function of enclosed area (measured in the disc plane). The result is in figure \ref{fig:QCTrajectories}. The behaviour seems to be independent of shape, as for small area they all have the same linear behaviour. The endpoints of the graphs is where the minima/wavefunctions would meet on the disc. The ellipse and square reach this point earlier than the circle and this is why they should also diverge earlier, as confirmed in the plot. This result confirms that the parametric fluctuations won't influence the acquired phase much.

Figure \ref{fig:QCTrajectories} also shows a circle not enclosing the centre has almost no geometric phase. This shows that for a geometric phase the disc centre needs to be enclosed. We have also done calculations for translated enclosing circles, but as soon as the disc centre is in the circle the line is the same as for the regular circle (for small area), again proving robustness against parametric fluctuations. The line for a circle through the disc centre is in between. 


\begin{figure}[hb!]
	\begin{center}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_Trajectories.pdf}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_Circles.pdf}
		\caption{The rotation angle on the Bloch Sphere for traversing one cycle on different trajectories as function of the enclosed area. The endpoints is where the minima meet. \emph{Left}: Different shapes (eqs. \eqref{eq:Circle}, \eqref{eq:Ellipse}, \eqref{eq:Square}) \emph{Right}: Shifted circles, i.e. the same radius but with the disc centre on it's edge or inside/outside by $R\cdot10^{-6}$, where $R$ is the radius. For small area the system is insensitive to shape and so to noise and we get a geometric phase as long as the disc centre is enclosed. (Parameters: $L_z=4,L_r=4/3, I_n=1$, eccentricity of the ellipse $e=0.94$.)}
		\label{fig:QCTrajectories}
	\end{center}
\end{figure}



