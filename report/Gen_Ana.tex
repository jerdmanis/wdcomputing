\chapter{Circular trajectory on isotropic disc}
\label{ch:Gen_Ana}



As a first experiment, the specific case of a circular trajectory through an isotropic disc (with the same centre as the disc) is examined. With isotropic we mean symmetric with respect to rotation around the easy-axis. This case is a simple example and is meant to give a basic understanding of the behaviour and possibilities of our system and help study more complex trajectories. We repeat the assumptions to emphasise the validit of our results. Everything is done in the quasi-classical regime $Q\gg1$. To obtain a holonomic transformation, we restrict ourselves to the limit $\Delta\ll\omega\ll\omega_0$, where $\Delta$ is the level spacing between the nearly degenerate states and $\omega_0$ is the harmonic oscillator level spacing.

In this thesis $\hbar$ and $2e$ are taken to be unity. The time-independent Hamiltonian for the Weyl disk is then given by

\begin{equation} \label{eq:Ham}
	H(\vec\phi^r) = \sum_{n=x,y,z} H_n(\phi^r_n), \qquad
	H_n(\phi^r_n) = I_n\hat{\phi}_n\sigma_n + \frac{(\hat{\phi}_n - \phi_n^r)^2}{2L_n} + \frac{\hat{Q_n}^2}{2C_n} .
\end{equation}
The external reservoir phases $\vec\phi^r$ are the parameters that are made time-dependent and will traverse a closed path through parameter space. The easy direction is taken to be along the $z$-axis by taking $L_z$ and $I_z$ larger than $L_{x,y}$ and $I_{x,y}$. The Hamiltonian can be split into two terms, involving the easy and the radial direction respectively, by setting $I_{x,y}=I_r$, $L_{x,y}=L_r$, $C_{x,y}=C_r$ (isotropy): $H = H_z+H_r$.
The time-dependent Hamiltonian is

\begin{equation} \label{eq:HamTD}
	H(t) = H(R_z(t)\vec{\phi^r}) = H_z(\phi^r_z)+H_r(R_z(t)\vec{\phi^r}) ,
\end{equation}
where $H_r(\vec\phi^r) = H_x(\phi_x)+H_y(\phi_y)$ and $R_z(t)$ is an operator rotating a vector by $\omega t$ around the $z$-axis.





\section{Rewriting Hamiltonian}



A cyclic state is a state which performs cyclic evolution as discussed in \ref{pr:Geom} about geometric phases. It is possible to find cyclic states in the adiabatic limit for the Weyl disk system, but for this the Hamiltonian will first be rewritten in the form $e^{-iAt}H(0)e^{iAt}$.





\subsection{Rotation symmetry of the Hamiltonian}



It will be shown that it is possible to rewrite \eqref{eq:HamTD} as two rotations, coordinate rotation and spin rotation. The rotation operators are then written as exponents. We start rewriting the Hamiltonian. Let the subscripts $R/R^{-1}$ indicate that vectors are rotated by $R_z(t)/R_z^{-1}(t)$. Then we can write

\begin{equation}
\begin{split}
	H_r(t) &= H_{r}(R_z(t)\vec{\phi^r}) = I_r(\hat{\phi}_x\sigma_x + \hat{\phi}_y\sigma_y)+\frac{(\hat{\phi}_x-\phi_{Rx}^r)^2+(\hat{\phi}_y-\phi^r_{Ry})^2}{2L_r}+\frac{\hat{Q_x}^2 + \hat{Q_y}^2}{2C_r} \\
	&= I_r(\hat{\phi}_x\sigma_x+\hat{\phi}_y\sigma_y) + \frac{(\hat{\phi}_{R^{-1}x}-\phi_x^r)^2+(\hat{\phi}_{R^{-1}y}-\phi_y^r)^2}{2L_r} + \frac{(\hat{Q}_{R^{-1}x})^2+(\hat{Q}_{R^{-1}y})^2}{2C_r} .
\end{split}
\end{equation}
Note that the sum of the squares of the $x$- and $y$-terms does not change when rotated. Furthermore the first term in the last line is rewritten

\begin{equation}
	I_r(\hat{\phi}_x\sigma_x+\hat{\phi}_y\sigma_y) = e^{-i\frac{\omega t}{2}\sigma_z} [I_r(\hat{\phi}_{R^-1x}\sigma_x+\hat{\phi}_{R^-1y}\sigma_y)] e^{i\frac{\omega t}{2}\sigma_z} .
\end{equation}
Here the $e^{\pm i\frac{\omega t}{2}\sigma_z}$ rotate the quasi-spin operators around the $z$-axis over an angle $\omega t$ and is a well known operator in quantum information theory. The spin rotation cancels the phase rotation. After these observations our Hamiltonian can be written \label{eq:HamRewritten}

\begin{equation} \label{eq:HamRot2}
	H(t) = H\left(R_z^t\vec{\phi^r}, \vec{\hat{\phi}}, \vec{\hat{Q}}\right)
	= e^{-i\frac{\omega t}{2}\sigma_z} H\left(\vec{\phi^r}, R_z^{-t}\vec{\hat{\phi}}, R_z^{-t}\vec{\hat{Q}}\right) e^{i\frac{\omega t}{2}\sigma_z} .
\end{equation}
At this point we have managed to prove that a rotation of the reservoir phases is equivalent to a rotation of the spins and a coordinate rotation (rotation of $\vec{\hat\phi}$ and $\vec{\hat Q}$).





\subsection{Coordinate rotation operator}



To reach our final form for the Hamiltonian, an operator is found for the rotations $R_z^{-1}(t)$.
By rotating the coordinates $\vec x\rightarrow R\vec x$ and defining the operator $\tilde U(R)$ to rotate coordinates of the wavefunction $\tilde U(R)\psi(\vec{x})=\psi(R\vec{x})$ it is possible to get from the eigen equation that 

\begin{equation} \label{eq:Utildephi}
\tilde U(R)\vec{\hat{\phi}}\tilde U(R^{-1}) = R^{-1}\vec{\hat{\phi}}.
\end{equation}
So we find that we can write the rotation of the phase operators using operators of coordinate rotation. The same can be done for $\vec{\hat{Q}} = i\vec\nabla$. It can be shown that $ \vec \nabla ' = R^{-1} \vec \nabla $.
Remark that $\vec \nabla$ and $\tilde U(R^{-1})$ commute. These two properties are necessary to make the same calculation as before.

%\begin{equation}
%\begin{split}
%R^{-1}\vec{\hat{Q}}\psi(\vec{x}) &= iR^{-1}\vec{\nabla}\psi(\vec{x}) \\
%R^{-1}\vec{\hat{Q}}\psi(R\vec{x}) &= i\vec{\nabla}\psi(R\vec{x}) \\
%R^{-1}\vec{\hat{Q}}\tilde U(R)\psi(\vec{x}) &= i\tilde U(R)\vec{\nabla}\psi(\vec{x}) \\
%\vec{\hat{Q}}\tilde U(R)\psi(\vec{x}) &= i\tilde U(R)R\vec{\nabla}\psi(\vec{x}) \\
%\tilde U(R^{-1})\vec{\hat{Q}}\tilde U(R)\psi(\vec{x}) &= iR\vec{\nabla}\psi(\vec{x}) = R\vec{\hat{Q}}\psi(\vec{x})
%\end{split}
%\end{equation}

\begin{equation} \label{eq:UtildeQ}
	\tilde U(R)\vec{\hat{Q}}\tilde U(R^{-1}) = R^{-1}\vec{\hat{Q}}.
\end{equation}

What remains is to find an expression for $\tilde U(R)$. In what follows the notation $\tilde U(\theta) = \tilde U(R(\theta))$ is used. For infinitesimal $\Delta\theta$ it holds that

\begin{equation}
	\tilde U(\theta + \Delta \theta) = \tilde U(\theta) + \frac{\partial\tilde U}{\partial\theta} \Delta\theta .
\end{equation}
If we fill in $\theta=0$ we get the operator for rotation over infinitesimal angle 
\begin{equation}
	\tilde U(0 + \Delta \theta) = \mathbb{1} + \left. \frac{\partial\tilde U}{\partial\theta}\right|_{\theta=0}\Delta\theta .
\end{equation}
This expression may be used to divide  rotations into many small rotations
\begin{equation}
	\tilde U(\theta) = \lim_{n\rightarrow\infty}\tilde U^n\left(\frac{\theta}{n}\right) = \lim_{n\rightarrow\infty}\left(\mathbb{1} + \left. \frac{\partial\tilde U}{\partial\theta}\right|_{\theta=0} \frac{\theta}{n}\right)^n .
\end{equation}
The solution is a well known result in mathematics and reads
\begin{equation}
	\tilde U(\theta) = e^{\left. \frac{\partial\tilde U}{\partial\theta}\right|_{\theta=0} \theta} .
\end{equation}
We define $i\hat{L}_z=\left. \frac{\partial\tilde U}{\partial\theta}\right|_{\theta=0}$ and write $\tilde U(\theta) = e^{i\hat{L}_z \theta}$.

Putting everything together the final expression for the Hamiltonian is

\begin{equation} \label{eq:Moore1}
H(t) = e^{-i\frac{\omega t}{2}\sigma_z} e^{-i\hat{L}_z \omega t} H\left(\vec{\phi^r}, \vec{\hat{\phi}}, \vec{\hat{Q}}\right) e^{i\hat{L}_z \omega t} e^{i\frac{\omega t}{2}\sigma_z}
\end{equation}
or referring to our desired form

\begin{equation} \label{eq:Moore2}
H = e^{-iAt} \tilde H e^{iAt}
\end{equation}
with $\tilde H = H\left(\vec{\phi^r}, \vec{\hat{\phi}}, \vec{\hat{Q}}\right)=H(0)$ and $A = (\sigma_z/2+\hat{L}_z)\omega$. Remark that only because $\sigma_z$ and $\hat L_z$ commute it is possible to add them in one exponent.





\section{Connection}





\subsection{Adiabatic}



The Schr\"{o}dinger equation for each of the degenerate eigenstates in adiabatic approximation is
\begin{equation} \label{eq:EqCirc}
	H(\phi(t)) \ket{\pm(t)} = e^{-iAt}H(0)e^{iAt}\ket{\pm(t)} = 0 .
\end{equation}
This expresses that at any time the eigenstate remains an eigenstate. We take the eigenenergy to be zero, because we are free to choose a null point. Since $H(0)\ket{\pm(0)} = 0$ it follows by substitution that

\begin{equation}
	\ket{\pm(t)} = e^{-iAt}\ket{\pm(0)}
\end{equation}
solves \eqref{eq:EqCirc}. Thus $U=e^{-iAt}$ is a holonomic transformation for circular trajectories. The $\ket{\pm(0)}$ are cyclic states.

With this solution we can evaluate the connection $M_\phi$ given by
%\Q{Why are there off-diagonal elements for cyclic states?}
\begin{equation} \label{eq:M2}
U(t) = e^{-\int_{0}^{\tau}{M(t)dt}}, \hspace{5pt}
M_\varphi(t) = \begin{bmatrix} \braket{+|\dot+} & \braket{+|\dot-} \\ \braket{-|\dot+} & \braket{-|\dot-} \end{bmatrix} .
\end{equation}
We have given an index $\varphi$ to the connection to indicate that only the angle on the disc changes during the (circular) trajectory. This will makes comparison later with section \ref{ch:QC_Ana} clearer. Combining this equation with
\begin{equation}
\ket{\dot\pm(t)} = -iA\ket{\pm(t)}
\end{equation}
we can evaluate connection $M$ for circular closed trajectories in an isotropic disc, which is
\begin{equation} \label{eq:M_adiabatic}
	M_\varphi = -i\begin{bmatrix} \braket{+|A|+} & \braket{+|A|-} \\ \braket{-|A|+} & \braket{-|A|-} \end{bmatrix} .
\end{equation}
It turns out the components of $A = (\sigma_z/2+\hat{L}_z)\omega$ have to be calculated for the evolution matrix. In the quasi-classical regime the minima both act as harmonic oscillators in the space of the $\phi$, as will be become clear in figure \ref{fig:overlap} in the next chapter. This enables us to simplify even further, for we only expect ground harmonic oscillator states and can assess the $L_z$ operator in this space. It can be derived, as done in appendix \ref{app:Lz}, that it is zero and can be discarded as long as this approximation holds. This means our operator is simply the quasi-spin operator $A = (\sigma_z/2)\omega$:
\begin{equation} \label{eq:M_adiabatic_appr}
	M_\varphi = -\frac{i}{2\omega}\begin{bmatrix} \braket{+|\sigma_z|+} & \braket{+|\sigma_z|-} \\ \braket{-|\sigma_z|+} & \braket{-|\sigma_z|-} \end{bmatrix} .
\end{equation}
Also this is confirmed by calculations of chapter \ref{ch:Gen_Num} in figure\ref{fig:RotCirc}. As we will see later, there are states in the minima which do not overlap, so \eqref{eq:M_adiabatic_appr} will be diagonal and these states are cyclic.




\subsection{Nonadiabatic}



A Hamiltonian of the form \eqref{eq:Moore2} has the solution
\begin{equation}
	U(t)=e^{-iAt}e^{-iBt} ,
\end{equation}
where $B=H(0)-A$. This can be seen by direct substitution into the Schr\"odinger equation. An elaborate treatment is in appendix \ref{sc:Moore}. In our case
\begin{equation} \label{eq:U_diabatic}
	U(t) = e^{-i(\sigma_z/2+L_z)\omega t}e^{-i(H(0)-(\sigma_z/2+L_z)\omega)t} .
\end{equation}

This is the general solution to our problem. Below we simplify it for the limit we are considering, but otherwise it is the complete evolution operator. It should be applied when we are off of the disc or not in the Weyl disk limit and degeneracy is broken, or when we are moving too fast and higher levels come into play.


%\subsection{Derivation of adiabatic evolution}

Equation \eqref{eq:U_diabatic} reduces to the previous adiabatic solution in the limit $\Delta\ll\omega\ll\omega_0$. Because $\omega\ll\omega_0$, the Hamiltonian $H(0)$ is much larger than $(\sigma_z/2+L_z)\omega$, so we neglect the latter:
\begin{equation} \label{eq:U_diabatic_limit}
	U(t) = e^{-i(\sigma_z/2+L_z)\omega t}e^{-i H(0)t}
\end{equation}
The second exponent corresponds to the dynamical phase factor, which is irrelevant for nearly degenerate levels with $\Delta\ll\omega$, and thus the first exponent corresponds to the geometric phase factor. Here we can again choose to only consider ground state harmonic oscillator functions to drop $L_z$, as in previous subsection.




%To verify, we may consider a two-level system and acquire the geometric phase from the holonomic computation framework. For a two-level degenerate system we have (...)
%\section{Evolution matrix as geometric phase}For a two-level degenerate system we have 

%The evolution operator in general can be derived in a different way by looking at the Berry phase of cyclic states \cite{Moore}, for which the reader is referred to the appendix. The result is equivalent to \eqref{eq:MCirc1}.
%Solutions are
%$$ U(t) = e^{-i\lambda_1 t}\ket{u_1}\bra{u_1} + e^{-i\lambda_2 t}\ket{u_2}\bra{u_2} $$
%$$ = \begin{bmatrix} e^{-i\lambda_1 t} & 0 \\ 0 & e^{-i\lambda_2 t} \end{bmatrix} \begin{bmatrix} a_1 \\ a_2 \end{bmatrix} $$




%\section{Symmetry considerations for the connection}
%
%There is a symmetry on the disc for the symmetry operator $I_xI_z\sigma_x$. One can see this by applying it on the eigenequation for the basis states $H(0)\ket{\pm}=E_\pm\ket{\pm}$. Applying the operation $I_xI_z\sigma_x$, where $I_n$ are the inversion operators flipping the wave function in the $n$-direction in phase space, on an eigenstate in the plane $\phi^r_z=0$  doesn't change the state $I_xI_z\sigma_x\psi=\psi$. One proves such symmetries by showing that the commutator with the Hamiltonian vanishes, $[H,I_xI_z\sigma_x]=0.$ From this symmetry it can be seen that the off-diagonal elements  of $M$ should be zero, since
%
%\begin{equation}
%	\braket{\pm|\sigma_z|\mp} = \braket{\pm|\sigma_xI_zI_x\sigma_zI_xI_z\sigma_x|\mp}
%	= -\braket{\pm|\sigma_z|\mp} .
%\end{equation}
%
%The symmetry is broken by either anisotropy or an offset $\phi^r_z \neq 0$ from the disc.
