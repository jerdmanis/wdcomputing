\chapter{Introduction}

\label{ch:Intro}



\section{Quantum computing}





\subsection{History}



Since the discovery of the Turing machine in 1936 (a "model of a computer" proposed by Turing) people have been wondering whether there are algorithms performed faster by some other system. Already in 1982 Feynman had a paper published about quantum computation, which was partly inspired by the difficulty of simulating quantum mechanics on classical computers \cite{Feynman}. This might be considered the start of quantum computing. In the early 1990's it was shown by several researchers that simulating quantum systems can be done efficiently on systems exploiting quantum physics. The most famous example is that in 1994 Shor demonstrated that two problems, finding prime factors for an integer and the discrete logarithm problem, are efficiently solved by quantum computers \cite{Shor}. These problems have an interesting application in public key cryptography, where prime-factoring can be used to find the private key and crack the code. However, they do not yet have an efficient algorithm on a classical computer. This is one of the problems that reveal that quantum computers have some advantages over classical ones.

This can also be considered part of the field of quantum communication. Next to the above-mentioned cracking of public keys there are other applications such as communicating using entangled quantum states containing information. States are moved from A to B by entanglement, thereby teleporting a quantum state from one system to the other, while making it impossible to eavesdrop on the channel which would disrupt the signal.

The field of quantum information is rapidly growing. Firstly, because of the promises it brings for solving computational problems with fast algorithms, for applications in artificial intelligence, for quantum communication and for other fields. Secondly, because of the limitations that now arise in the fabrication of classical computers. For decades Moore's law has remained valid and the computational power approximately doubled every two years. But now dimensions have become so (microscopically) small that quantum mechanical effects make it hard to build ever smaller devices. The classical limits are near, whereas the quantum world sometimes seems unlimited in possibilities. Now scientists are exploring what is possible.

The promise of having a full-scale quantum computer has been driving the physics of quantum control in the field for the last two decades. At present, some small quantum computers have been built as a result. Often these are designed for a specific purpose, such as simulating heat transport through an atomic system as has been done by Harvard, MIT, CalTech \cite{TAQ}. More general quantum computing is still out of reach. Implementation of qubits and of means to probe those qubits is a present-day problem. Several challenges (treated in the next subsection) lie in creating robust systems fit for actual computation. That is why at present possibilities of making feasible systems are still limited and the state of the art systems only contain around 50 qubits of good quality (functional qubits, for instance, suffer less from noise).

Today only some quantum computing resources are available through the cloud and are accessed by research facilities. But several competitors are in the race of overcoming these obstacles and building more useful quantum computers, discussed in section \ref{sc:SotA}





\subsection{Qubits}



Qubits are the quantum analogue to classical bits. They are systems of two quantum states representing the "0" and "1". Many candidates have been found by scientists for serving as qubits. The simplest example might be a spin-1/2 particle such as an electron, in which case the spin-up and spin-down states are used as "1" and "0". As opposed to a classical bit, the qubit can be in a superposition of states. This changes the way of doing computation and brings new features and possibilities.

\begin{figure}
	\begin{center}
		\includegraphics[width=0.25\textwidth]{figures/Bloch.png} \\
		\caption{Bloch sphere. Every point represents a state of the form, where the overal phase is disregarded, because it is not important for quantum computation. \cite{BlochSphere}}
		\label{fig:Bloch}
	\end{center}
\end{figure}

It is convenient in quantum information to map the state of a qubit onto the Bloch sphere, a unit sphere around the origin, where the bottom is the $\ket{0}$  state and the top is the $\ket{1}$ state (figure \ref{fig:Bloch}). The mapping is given by

\begin{equation}
	(\theta,\phi) \rightarrow \cos(\theta/2)\ket{0} + e^{i\phi} \sin(\theta/2) \ket{1} ,
\end{equation}
where $\theta=0..\pi$ and $\phi=0..2\pi$. Manipulations of the state are SO(3)-operations (3D rotations around the origin) and so can be represented by rotations on the Bloch sphere. They are written as SU(2)-matrices. Some simple quantum logic gates interesting for computation are the phase gate $R_\phi$ adding a phase $e^{i\phi}$ to the $\ket{1}$ state, which is equivalent to a rotation around the z-axis on the Bloch sphere, the Pauli gates given by the Pauli matrices rotating by $\pi$ around the corresponding axis, the Hadamard gate mapping the $\ket{0}$ and $\ket{1}$ states to respectively the mixed states $\frac{\ket{0}+\ket{1}}{\sqrt{2}}$ and $\frac{\ket{0}-\ket{1}}{\sqrt{2}}$. There are also multi-qubit operations such as the swap gate swapping the occupations of two levels. As an example of implementation, for the manipulation of the electron spin a magnetic field might be applied to rotate the spin around the field axis. For full quantum computation a \emph{universal set of gates} is necessary, i.e. a set of gates from which all others can be constructed.

The important aspects of qubit systems are noise, control and scalability. One wants to be able to control the evolution of the qubit to implement quantum gates, i.e. to perform operations on it. Noise can interfere and complicate this. Think of fluctuations of the magnetic field strength or axis in the spin example. Finally, if a qubit allows for good computation, one would want to scale up to build computers of many qubits working together.





\subsection{Superconductivity}



A lot of qubits involve superconductivity. This is, simply put, the free movement of electrons without any resistance, which occurs at low temperatures in metals and also at higher temperatures up to over 130K in some exotic materials. This is caused by a pairing up of electrons into so called Cooper pairs.

Classically this can be explained as follows. Electrons in a conductor repell each other when they come close. Therefore one expects that electrons always want to be as far away from other electrons as possible. However, this doesn't take into account the interaction with the lattice of the metal. Electrons move through the lattice and interact with it, since the lattice has positive charge. The lattice is slightly deformed such that the electron is surrounded by some positive charge. This counters the repulsion between electrons with a attractive force, which can overcome the repulsion at long distances of typically hundreds of nanometers.

Superconductivity is, however, a quantum phenomenon and the rigorous treatment shows it is caused by electron-phonon interaction, where the phonon represents the collective motions of positive ions in the lattice. Cooper pairs consist of two fermions and are thus bosons and can occupy the same state. A certain range of the energy spectrum is condensed into a single state and this leaves an energy gap $\Delta$. This is the binding energy of a pair and is in the order of millielectronvolts. This is easily overcome by temperatures that are not extremely low and superconductivity disappears if it is not cold enough. This energy gap also explains the absence of resistance, since exciting the fluid of cooper pairs costs an minimum of energy equal to the binding energy and this can not be paid at low temperatures.




\subsection{Qubit experiments}

What makes a feasible qubit? The answer was already mentioned in the last subsection. It depends on several aspects and is a careful trade-off between them. Every implementation has weaknesses, but the aim is to make a system functioning despite them. Here we elaborate on the characteristics and describe how they can be measured.

The most conclusive measurement of effective quantum manipulation is that of Rabi oscillations, oscillations between high occupation in either state, where the occupations vary as function of a rotation parameter. This is done by rotating on the Bloch sphere. For example, the spin of an electron can be rotated by a magnetic field, which is equivalent to a rotation on the Bloch sphere. The is what one would like to achieve to prove a system fit for operation. 

A great obstacle is often the inevitable noise. There are several sources of noise. For instance, fluctuations can slightly alter the Hamiltonian and interfere with the evolution of the system. This could result in dephasing of the two states. In quantum computation there are ways to deal with noise and nonetheless achieve useful calculation. So-called quantum \emph{error correcting code} can make for fault-tolerant quantum computation. However, this correction is only feasible above a certain noise threshold, which is often higher than the practical systems allow. Thus noise doesn't make computation impossible, but it should be minimised.

Two important quantities in this respect are the relaxation and coherence time. If there is an energy difference between the two states, the system will relax into the lowest state. Calculations have to be done in times much shorter than the relaxation time to produce useful results. The same holds for decoherence. States need to remain coherent, for also the phase is important in computation. If for example the energies of the levels slightly fluctuate, then the difference in dynamical phase could cause decoherence.

A measure for coherence time can be extracted from the Gaussian decay of so-called Ramsey fringes. Ramsey fringes are the oscillatory behaviour of the occupation of the levels after applying a $\pi/2$-pulse, waiting a time $\tau$ and applying a $\pi/2$-pulse again as function of the pause $\tau$. So first the system is brought to an even superposition (on the equator of the Bloch sphere), then during the pause the qubit goes around the equator, and at some point it is rotated back along the same axis as before. This gives oscillatory behaviour as function of the pause length, but decoherence randomises the phases and results in a decrease in amplitude (figure \ref{fig:AQPlots}).
 
 
 
 

\subsection{State-of-the-art quantum computers}
\label{sc:SotA}



There are many systems eligible for qubit purposes. Most are solid state qubits, which involve electron transfers. Depending on the implementation, many quantities can provide the degree of freedom for the qubit, such as charge or superconducting phase or flux \cite{QT}. Consequently qubits can currently be made out of almost anything. The last decades lots of quite different systems have been competing in becoming the most promising technique. We list the most important developments. At IBM, Google, Intel and Rigetti superconducting circuit loops are used \cite{SCQs}. At QuTech in Delft scientists are currently working on a topological quantum computer \cite{QuTech}. Their qubits are topologically protected from noise, which gives a great advantage. They are using Majorana particles in a superconducting nanowire for this. IonQ manipulates trapped ytterbium ions with lasers and remarkably do that at room temperature \cite{IonQ}. The company D-Wave Systems uses a very different technique called quantum annealing to do calculations with thousands of qubits, which is already being used, but has not yet beaten classical computers \cite{D-Wave}. Xanadu is also working on a room-temperature system based on photon states \cite{Xanadu}.

Some qubit systems are quantum simulators, meaning they only simulate a certain quantum system and are not for general purpose. Several quantum simulation problems have been proposed \cite{QSim1,QSim2,QSim3}. We give two examples of simulators. Within the team Harvard, MIT and CalTech atoms are trapped in an optical field and manipulated by using lasers \cite{TAQ}. They exploit the highly excited Rydberg states of the atoms. In Maryland a group has been working on something similar also with an optical lattice, but they use trapped ions instead and the hyperfine states of the spins \cite{TIQ}. 

In spite of great efforts from many parties, a qubit reliable for large scale quantum manipulation has not yet been found. Recently Andreev bound states have been studied for qubit purposes and they could be an answer to the current issues \cite{ALQ1,ALQ2,ALQ3}. In the present paper we treat a topological qubit based on Andreev levels, which should also be protected from noise.





\subsection{Andreev qubit}
\label{ch:AndQbt}


Now it is time to get more specific about the subject of this thesis. We want to make an Andreev qubit. It uses two Andreev bound states (ABS), quasiparticle states in a normal region between two superconducting materials, also known as a Josephson junction. These states emerge as follows. At the barriers between the superconductor and the scattering region something called Andreev reflection can take place: if an electron in the scattering region has insufficient energy to enter the superconductor, it is reflected back as a hole. This implies charge transfer between the scattering region and the superconductor and implies the creation of a Cooper pair. This supercurrent is called Josephson current. Vice versa for a hole meeting a barrier. If the electrons/holes have insufficient energy to go to either superconductor, this constitutes bound quasiparticle states giving rise to a discrete energy spectrum and  quasiparticles called Bogoliubons. Each transport channel in the scattering region gives rise to an Andreev bound state. Its energy depends on the transmission of the channel and the superconducting phase difference between the two superconductors. One can tune the ABS with these parameters to achieve a desirable qubit system. 

Andreev level qubits have recently been an active subject of study. Often ABS are created using a weak link, which is a short region where superconductivity is weakened or broken and where the Josephson effect can take place. In 2002 a group proposed a system using a quantum point contact (QPC) in a superconducting ring to create ABS, where the QPC acts as a quantum dot \cite{ALQ1}. The Andreev levels are coupled to the persistent Josephson current in the ring, which (through magnetic flux) can be measured to read out the Andreev qubit or can be used to manipulate the qubit. Also in 2002 others have suggested using the ABS levels in a SINIS-junction (superconductor - insulator - normal metal - insulator - superconductor) and controlling these by means of the voltage across one of the barriers and a separate transport current \cite{ALQ2}.

More recently an experiment has been done on an Andreev level qubit, demonstrating impressive features \cite{ALQ3}. Their setup is a superconducting aluminium loop with a weak link in the form of an atomic contact. They created the contact using the micro-fabricated break-junction technique: the aluminium loop with a narrow constriction is put on a controllably bendable substrate, the substrate is bent until the bridge breaks, and further bending allows tuning of the transmission probability of its channels.
The ABS spectrum has a ground and excited state and two intermediate states, which they show don't interfere. Tuning is done using a magnetic field: the magnetic flux through the loop determines the phase drop across the contact through $\delta=2\pi\phi/\phi_0$ with $\phi_0$ the flux quantum.
An inductively coupled microwave resonator is employed to excite and probe the qubit through spectroscopy (figure \ref{fig:AQsetup}).

\begin{figure}
\begin{center}
	\includegraphics[width=0.2\textwidth]{figures/Andreev_qubit_setup.png} \\
	\caption{Setup for Andreev qubit. A loop with a weak link (C) is coupled to an inductively coupled microwave resonator (B). This in turn is probed by reflectometry (A). \cite{ALQ3}}
	\label{fig:AQsetup}
\end{center}
\end{figure}
	
Some of the results they have produced are Rabi oscillations and finding the relaxation and coherence time (figure \ref{fig:AQPlots}). The coherence time is measured by looking at Gaussian decay of the Ramsey fringes. \emph{The short coherence time was found to be caused by fluctuations of the energy levels, which in turn depends on the magnetic flux and the transmission.} Fluctuating transmission can be the result of mechanical vibrations or motions of the atoms close to the contact. The conclusion of the paper is that it is a proof of concept, but further investigation is needed into the sources of decoherence and scalability.

\begin{figure}
\begin{center}
	\includegraphics[width=0.9\textwidth]{figures/A_Rabi_oscillations.png} \\
	\includegraphics[width=0.48\textwidth]{figures/D_Relaxation_time.png}
	\includegraphics[width=0.48\textwidth]{figures/F_Ramsey_fringes.png} \\
	\caption{Red line indicates the excited state, blue line the ground state and the green line the intermediate states (which are parasitic and not part of the qubit). Plot \emph{A} shows Rabi oscillations, plot \emph{B} relaxation and plot \emph{C} Ramsey fringes measuring coherence time. The short coherence time is blamed on fluctuations of Andreev levels. \cite{ALQ3}}
	\label{fig:AQPlots}
\end{center}
\end{figure}





\section{Holonomic quantum computation}



Lately a new technique has enjoyed some attention for it's desirable features and notably it's resistance to noise. It employs the phase a system acquires by moving around a loop in parameter or projective Hilbert space. There have been already quite some proposals for physical systems implementing this geometric phase \cite{HQC-Ex1,HQC-Ex2,HQC-Ex3,HQC-Ex4,HQC-Ex5}. 



\subsection{Geometric phase}

\label{pr:Geom}

%So we are interested in closed trajectories of our Hamiltonian. 
Let's say that a Hamiltonian is a periodic function of time and that there are one or more states that undergo cyclic evolution, i.e. the state has only acquired a phase factor at the end of the period. Then these states are called cyclic states and their acquired phase consists of a dynamical and a geometric part. The dynamical phase is due to the energy of the system, but the geometrical phase depends on the path the state follows in Hilbert space. It is therefore considered a property of the spatial geometry. 

To see how this geometric phase appears, consider Schr\"{o}dinger's equation for time-dependent and periodic Hamiltonian, so $H(\tau)=H(0)$ \cite{Geo_Phase}.

\begin{equation} \label{eq:SE}
	H(t)\ket{\psi(t)} = i\hbar \frac{d}{dt} \ket{\psi(t)}
\end{equation}
Take $\psi(t)$ normalised and cyclic such that $\ket{\psi(\tau)} = e^{i\phi} \ket{\psi(0)}$ with $\phi$ real. Now define $\ket{\tilde\psi(t)} = e^{-if(t)} \ket{\psi(t)}$ such that $f(\tau)-f(0)=\phi$ and so $\ket{\tilde\psi(\tau)} = \ket{\tilde\psi(0)}$. Filling in in \eqref{eq:SE} gives

\begin{equation}
	\frac{df}{dt} = -\frac{1}{\hbar}\braket{\psi(t)|H(t)|\psi(t)} + \braket{\tilde\psi(t)|i\frac{d}{dt}|\tilde\psi(t)}
\end{equation}
Now using this and defining the geometric phase as the total phase minus the dynamical phase,

\begin{equation}
	\beta \equiv \phi + \int_{0}^{\tau} \braket{\psi(t)|H(t)|\psi(t)} dt ,
\end{equation}
it follows that

\begin{equation} \label{eq:geoPhase}
	\beta = \int_{0}^{\tau} \braket{\tilde\psi(t)|i\frac{d}{dt}|\tilde\psi(t)} dt
\end{equation}
Now consider the projective Hilbert space $\mathcal{P}$, that is the image of a map, mapping states differing only by a complex phase to the same element. For every curve $\tilde C$ in $\mathcal{P}$, which is the projection of the curve $C$ traversed through the Hilbert space $\mathcal{H}$, we can choose a function $f(t)$ such that we get the same $\tilde\psi(t)$ and thus the same $\beta$. Also \eqref{eq:geoPhase} shows that $\beta$ is independent of parameter $t$. It can be concluded that $\beta$ is a geometric property of the curve $\tilde C$ in $\mathcal{P}$ only.

Now consider the adiabatic limit, i.e. we change our Hamiltonian slow enough such that if a wavefunction is in an eigenstate it stays in the (moving) eigenstate \cite{AT}. In adiabatic evolution the cyclic states are thus eigenstates of the Hamiltonian. If we plug those in \eqref{eq:geoPhase}, we obtain the formula for Berry phase. A visual picture of this phase comes from rewriting this as a surface integral over the Berry curvature in parameter space \cite{Berry_Phase}, if we consider the time dependence of the Hamiltonian through some parameters $\vec R(t)$

\begin{equation} \label{eq:BerryPhase}
\beta = \int_{0}^{\tau} \braket{m(t)|i\frac{d}{dt}|m(t)} dt = \int_{0}^{\tau} \braket{m(t)|\nabla|m(t)} d\vec R(t) = \int_A \braket{\nabla m(t)|\times|\nabla m(t)} dS.
\end{equation}

%The geometric phase becomes distinguable in a degenerate subspace, within which dynamical phases are equal and we may choose to set $H(t)\phi=0$.
In this thesis we are mainly interested in adiabatic evolution of the state within a degenerate subspace, where $H(t)\ket{\psi}=0$ (zero point is arbitrary). The subspace is then spanned by cyclic states, each having a geometric phase factor. In this case dynamical phases are the same for each level, thus we neglect them. This allows us to obtain the unitary operator $U$ known also as a holonomic transformation \cite{Geo_Phase}:
%Such a case can also be analysed with geometric phases, as that can be considered a limit of separate levels, where the level spacing goes to zero.In this case the unitary operator, also known as holonomic transformation, in a cyclic basis is
\begin{equation}
	U=\begin{bmatrix} e^{i\beta_1} \\ & \ddots \\ && e^{i\beta_n} \end{bmatrix} = \mathcal{P} \exp \int_{0}^{\tau} \begin{bmatrix} i\braket{\tilde\psi_1|\dot{\tilde\psi}_1} \\ & \ddots \\ &&  i\braket{\tilde\psi_n|\dot{\tilde\psi}_n} \end{bmatrix}dt ,
\end{equation}
where $\mathcal{P}$ is the path-ordered product operator, which is out of scope here. The matrix is called the connection $M$ and we write
\begin{equation} \label{eq:U-M}
U = \mathcal{P}e^{\int_{0}^{\tau} M(t)dt} .
\end{equation}
A simple $2\times2$ case is derived in appendix \ref{sc:Con}. We might want to use a different basis and wonder if we can adjust our connection to give the right result for our basis, but this is not easily done. The resulting evolution operator should be the same as when we change basis afterwards. The integral prohibits simply transforming the connection to achieve this \footnote{We can split the integral into infinitesimal parts $\int_0^\tau Mdt = \int_0^{t_1} Mdt + \int_{t_1}^{t_2} Mdt + \dots + \int_{t_{n-1}}^\tau Mdt$ and approximate the exponent as $\exp(\int_0^\tau P(t)M(t)P^\dagger(t)dt) = P(0)\exp(\int_0^{t_1}Mdt)P^\dagger(0) \dots P(t_{n-1})\exp(\int_{t_{n-1}}^\tau Mdt)P^\dagger(t_n-1)$. We try to rewrite the transformation matrix products $P^\dagger(t_k)P(t_{k+1})$ using Taylor expansion of the second matrix as $(\mathbb{1}+P^\dagger(t_k)\frac{dP}{dt}\Delta t+\dots)$. The second term should be zero to justify simply changing basis of the connection and then integrating.}.

%Applicability depends on knowing the cyclic states, but fortunately it can be implemented for arbitrarily chosen basis (for every point in time) as long as it satisfies $\ket{m(t)}=\ket{m(t+T)}$ and changes continuously in time. Within the new space of alternative basis states this matrix is written
%where $M_{mn} = \braket{m|\dot {n}}$ is called the connection and $\ket{n}$ is an alternative basis state. These more complex adiabatic evolutions in degenerate subspaces have a structure called holonomy. Hence the name holonomic computation for the application of the Berry phase for qubit manipulation.





%\subsection{Berry phase}



%For our qubit we assume adiabatic evolution. This involves the approximation that the state of a system follows the eigenstates of the time-dependent Hamiltonian. One might look at it like this: the coefficients of the eigenstates do not change, only the eigenstates themselves due to the time-dependent Hamiltonian. This has proven to be true for infinitely slow evolution of a system, whose spectrum consists of a finite number of disjoint components for all time \cite{AT}.
%
%For cyclic adiabatic evolution of non-degenerate eigenstates, the eigenstates only acquire a geometric phase factor called Berry phase. This is a limiting case for the situation in the previous paragraph. In adiabatic limit a system in an eigenstate remains in the corresponding eigenstate:
%
%\begin{equation}
%	\hat H(\vec R) \ket{n(\vec R)} = E_n(\vec R) \ket{n(\vec R)} ,
%\end{equation}
%where $\ket{n}$ is a normalised eigenstate and time dependence is through the parameters $\vec R=\vec R(t)$. There is room to choose a phase factor for each point in parameter space, but $\ket{n(\vec R)}$ must be defined single-valued on the path through parameter space. The time evolution is then 
%
%\begin{equation}
%	\ket{\psi(t)} = e^{-i/\hbar \int_{0}^{t} E_n(\vec R(t'))dt'} e^{i\gamma_n(t)} \ket{n(\vec R(t))}
%\end{equation}
%Plugging this into the Schr\"{o}dinger equation \eqref{eq:SE} yields
%
%\begin{equation}
%\begin{split}
%	\dot \gamma_n &= i\braket{n(\vec R(t))|\nabla_R n(\vec R(t))} \dot {\vec R} (t) \\
%	\gamma_n &= i\int_C\braket{n(\vec R(t))|\nabla_R n(\vec R(t))} d\vec R
%\end{split}
%\end{equation}
%This result is a specific case of the geometrical phase. If we use eigenstates in \eqref{eq:geoPhase} we get the same result. This means that the adiabatic limit, however, is not needed apparently. 


Holonomic computation has several benefits. The nonabelian Berry phase can be expressed as a line integral through the parameter space, but it is also possible to transform it into an integral over a surface enclosed by the loop using Stoke's theorem. This emphasizes the geometrical nature and means that the phase is almost only dependent on the area enclosed by the loop \cite{HQC}. It also makes the qubit insensitive to random noise to first order in the driving of the evolution \cite{HQC,Noise_Area}.

Another benefit is that we can do computation in degenerate subspace. Thus the dynamical phase factor doesn't play a role any more. This is namely the same for both states. Any noise in this phase is therefore also absent \cite{HQC}. However, adiabaticity constrains the time frame of computation \cite{HQC_NA}. Not only does this limit speed, but also becomes an issue when decoherence time doesn't exceed this time. For non-adiabatic evolution things are the other way around, fast evolution but with disturbence from dynamical phases.

Lastly, there is no energy relaxation between the levels, because they are degenerate and the relaxation process requires certain energy difference \cite{HQC}. This excludes bit-flip errors (unwanted occupation changes).

%The precise evolution of the system is not very important for the phase. 
%General resistance? 
%First of all, since a degenerate eigenspace is used, the dynamical phase factor doesn't play a role any more. This is namely the same for both states. Any noise in this phase is therefore also absent. (Bit-flip error)







