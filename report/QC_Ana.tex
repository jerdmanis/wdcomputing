\chapter{Quasi-classical approach for general trajectories}
\label{ch:QC_Ana}



The aim in this chapter is to find a recipe for calculating the evolution matrix of any trajectory in the quasi-classical limit. We still consider an isotropic disc for simplicity. For general trajectories it is possible to find an expression for calculating the matrix $M$ of \eqref{eq:U-M} within the quasi-classical approach. We do this by separately considering the effects of moving in the radial and tangential direction. Consider a parameterisation of a trajectory $(r(t),\varphi(t))$ in the Weyl disk, where $r$ and $\varphi$ are the radius and angle in the $x,y$-plane. Using the chain rule, the time derivative is expressed by
%\Q{Do we not include dynamical phase in eq \eqref{eq:dPsidt}?}
\begin{equation} \label{eq:dPsidt}
	\frac{d\ket{\Psi}}{dt} = \frac{\partial\ket{\Psi}}{\partial r}\frac{dr}{dt} + \frac{\partial\ket{\Psi}}{\partial\varphi}\frac{d\varphi}{dt} .
\end{equation}
The derivatives $\frac{\partial\ket{\Psi}}{\partial r},\frac{\partial\ket{\Psi}}{\partial\varphi}$ are needed to calculate arbitrary trajectories. We are looking for matrices $M_{r,\phi}$ that give us these derivatives
\begin{equation}
	\frac{d\ket{\Psi}}{dt} = M_r\ket{\Psi}\frac{dr}{dt} + M_\varphi\ket{\Psi}\frac{d\varphi}{dt} ,
\end{equation}
in which case $M=M_r\frac{dr}{dt} + M_\varphi\frac{d\varphi}{dt}$. With these matrices we would be able to investigate any trajectory. We will evaluate these matrices as $2\times2$ in our qubit space, again for the $\ket{\pm}$ states.

To be able to calculate the dependence of the connection on our parameters, we need to make some simplifications first based on the quasi-classical limit. We make statements about the energy and the spin behaviour from a classical point of view.

We will again consider the Hamiltonian with reservoir phases somewhere on the plane of the disc and take again $I_n=1$.  We drop the charge terms, which as conjugate variables for the $\hat\phi$ make for a broadening of the wavefunction and are thus not influential here. This could be considered the limit of very large capacity. We put $\phi_x^r=r$ and $\phi^r_{y,z}=0$ and write
\begin{equation} \label{eq:HamQC}
	H = \vec{\hat\phi}.\vec\sigma + \frac{(\hat{\phi}_x-r)^2}{2L_r} + \frac{\hat\phi_y^2}{2L_r} + \frac{\hat\phi_z^2}{2L_z} % + \frac{\hat{Q_x}^2+\hat{Q_y}^2+\hat{Q_z}^2}{2C}. 
\end{equation}




\clearpage
\section{Quasi-classical approach}

Because we are in the quasi-classical regime, we can make two simplifications based on results of the previous chapter to predict the behaviours of our qubit. One concerning energy and the other concerning spin.




\subsection{Energy condition}

\begin{figure}[ht]
	\begin{center}
		\includegraphics[width=0.3\textwidth]{../plots/Plot_Pot_1.pdf}
		\includegraphics[width=0.3\textwidth]{../plots/Plot_Pot_2.pdf}
		\includegraphics[width=0.3\textwidth]{../plots/Plot_Pot_3.pdf} \\
		\caption{The potential of \eqref{eq:E_cl} in phase space for phase along the radial axis and $z$-axis. \emph{Left}: $\phi^r_x=0.5$, \emph{Middle}: $\phi^r_x=1$ \emph{Right}: $\phi^r_x=2$. The results in figure \ref{fig:overlap} that the wavefunctions should move towards each other for increasing radius on the disc are confirmed. (Parameters: $L_z=4,L_r=4/3$)}
		\label{fig:Potential}
	\end{center}
\end{figure}


We have seen that the eigenstates have some classical behaviour in that they are localised and reside in the minima. A quasi-classical energy expression is therefore justified by replacing the operators $\hat\phi_n$ with their mean value $\phi_n$ and the quasi-spin part $\hat\phi_n\sigma_n$ with one of its eigenvalues $\pm\sqrt{\phi_n\phi_m}$ (for $\sigma=\pm1$).  What is left is
\begin{equation} \label{eq:E_cl}
	E_{cl,-} = -\sqrt{\phi_x^2+\phi_z^2} + \frac{({\phi}_x-r)^2}{2L_r} + \frac{\phi_y^2}{2L_r} + \frac{\phi_z^2}{2L_z} ,
\end{equation}
where we have substituted scalar $E$ for the operator $H$ in \eqref{eq:HamQC}. For the minimum holds
\begin{equation}
\begin{split} \label{eq:minPhi1}
	\pder{E}{\phi_r} &= \frac{\phi_r-r}{L_r} - 2\phi_r\frac{1}{2\sqrt{\phi_r^2+\phi_z^2}} = 0 \\
	\pder{E}{\phi_z} &= \frac{\phi_z}{L_z} - 2\phi_z\frac{1}{2\sqrt{\phi_r^2+\phi_z^2}} = 0 .
\end{split}
\end{equation}
Beginning with the second line of \eqref{eq:minPhi1} it is deduced that
\begin{equation} \label{eq:min1}
	(\phi_z=0 \land \phi_r\neq0) \lor L_z = \sqrt{\phi_r^2+\phi_z^2} .
\end{equation}
The solution with $\phi_z=0$ corresponds to the maximum and is dropped. Using this in the first line of \eqref{eq:minPhi1} we arrive at the expression for coordinate $\phi_r^0$ of the minimum
\begin{equation} \label{eq:phi0_r}
	\phi_r^0 = \frac{r}{1-L_r/L_z} .
\end{equation}
Inserting this in \eqref{eq:min1} gives the other coordinate $\phi_z^0$
\begin{equation} \label{eq:phi0_z}
	(\phi_z^0)^2 = L_z^2 - \left(\phi^0_r\right)^2 = L_z^2 - \left( \frac{r}{1-L_r/L_z} \right)^2 .
\end{equation}
These are the coordinates of the two minima, where we will assume the cyclic wavefunctions reside. In figure \ref{fig:Potential} a 2D slice of the quasi-classical potential is displayed for three values of $\phi^r_x$. It can be seen that the minima move towards each other for larger $\phi^r_x$ and eventually merge. The same can be seen in figure \ref{fig:spinCond}. It agrees with our earlier result in figure \ref{fig:overlap}. There the wavefunctions also became wider, which can be explained by the vanishing of the barrier between the minima. As long as the barrier height is well above the ground state energy of the harmonic oscillators in the minima, the wavefunctions remain localised.


\clearpage


\subsection{Spin condition}



\begin{figure}[ht]
	\begin{center}
		\includegraphics[height=150pt]{figures/Spin_Cond_2.pdf}
		\includegraphics[height=150pt]{../plots/Plot_theta(r).pdf}
		\caption{\emph{Left:} The $\phi_x,\phi_z$-plane with the minima for opposite $\phi^0_z$ and the spin vectors for the $\ket{\pm}$ states in opposite directions. The angle $\theta$ indicates how far off the direction of the minima is from the $z$-axis. \emph{Right}: A plot of the angle $\theta$ versus the scaled radius on the disc $\rho=r/(L_z-L_r)$ made using \eqref{eq:phi0_r} and \eqref{eq:phi0_z}. The relation is $\theta=\arcsin(\rho)$. Here it can also be seen from the angle $\pi/2$ at $\rho=1$ that the minima merge in our quasi-classical approximation.}
		\label{fig:spinCond}
	\end{center}
\end{figure}

\begin{figure}
	\begin{center}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_spin_phi_x(radius).pdf}
		\includegraphics[width=0.4\textwidth]{../plots/Plot_spin_phi_z(radius).pdf} \\
		\caption{Comparison of the directions of the spin and of the phase in the minimum versus distance from the centre of the disc, assuming $\phi^r_y,\phi^r_z=0$. The Hamiltonian is \eqref{eq:Ham}. \emph{Left:} The radial components of spin and phase are \emph{left} and the $z$-components are \emph{right}. The spin follows the phase of the system, although not exactly, and so verifies the soundness of \eqref{eq:SpinCond1}. (The number of harmonic basis functions used is $20^3$. Parameters are $L_z=4, L_r=4/3, C_n=20, I_n=1$.)}
		\label{fig:SpinPhi}
	\end{center}
\end{figure}


To minimise the energy term $\vec\phi\cdot\vec\sigma$ the spin is considered to be constrained to point in opposite direction to the minima, which are determined by the reservoir phases. This allows us to write
\begin{equation} \label{eq:AppFixedSpin}
	\ket{\Psi}(\phi_x,\phi_y,\phi_x^r,\phi_y^r)=\psi(\phi_x,\phi_y,\phi_x^r,\phi_y^r) \begin{bmatrix}s_+(\phi_x^r,\phi_y^r)\\s_-(\phi_x^r,\phi_y^r)\end{bmatrix} .
\end{equation}
Our assumption has been verified numerically to reasonable agreement in figure \ref{fig:SpinPhi}. The spin follows the phase vector closely and therefore we state the following spin condition. The position of the minima are written
\begin{equation} \label{eq:Minimum}
\begin{split}
	\vec\phi^0_{\pm} &= \begin{bmatrix} \phi^0_x \\ \phi^0_y \\ \pm\phi^0_z \end{bmatrix} ,
\end{split}
\end{equation}
where $\phi^0_z>0$. It is expected that the spin behaves as to minimise the first term of the Hamiltonian \eqref{eq:HamQC}. The spin is then determined by the condition
\begin{equation} \label{eq:SpinCond1}
	\frac{\vec\phi^0_\pm.\vec\sigma} {\left|\vec\phi^0\right|} \ket{S} = -\ket{S}
\end{equation}
which we may also write as
\begin{equation} \label{eq:SpinCond2}
	(\cos(\pm\theta)\sigma_z +\sin(\theta)\sigma_x) \ket{S} = -\ket{S} ,
\end{equation}
where $\ket{S}$ is the spin of an eigenstate and $\theta$ is the angle the vector $\vec\phi^0$ makes with the positive z-axis clockwise (figure \ref{fig:spinCond}). This means the spin is antiparallel to the phase vector. This may be verified by filling in into \eqref{eq:SpinCond2} the spins
\begin{equation} \label{eq:SpinStates}
\begin{split}
	\ket{S_+} &= e^{i\sigma_y\theta/2}\ket{\uparrow} \\
	\ket{S_-} &= e^{-i\sigma_y\theta/2}\ket{\downarrow} .
\end{split}
\end{equation}
Here the exponent rotates the quasi-spin and thereby our two levels over the Bloch sphere to anti-align them with the $\vec\phi^0$. The $\ket{S_\pm}$ states are the spins of the $\ket{\mp}$ states. By collapsing (\ref{eq:SpinCond2}) and taking note of

\begin{equation} \label{eq:spinExp}
\begin{split}
	e^{i\sigma_y\theta/2} \sigma_x e^{-i\sigma_y\theta/2} &= \cos(\theta)\sigma_x +\sin(\theta)\sigma_z \\
	e^{i\sigma_y\theta/2} \sigma_z e^{-i\sigma_y\theta/2} &= \cos(\theta)\sigma_z -\sin(\theta)\sigma_x ,
\end{split}
\end{equation}
we see that the equation is satisfied.





\section{Calculate $M$}



The derivatives are calculated with the product rule to account for changes in the wavefunction and spin
\begin{equation} \label{eq:rDer}
	\frac{d\ket{\Psi}}{dx} = \frac{d\psi}{dx}\ket{S} + \psi\frac{d}{dx}\ket{S}.
\end{equation}
The term with derivative of the wave function $d\psi/dx$ doesn't contribute. To see this, collapse the term with $\ket{\Psi}$ and use partial integration. We have approximated the wavefunctions by only the ground state harmonic oscillator function in the respective wells, which is real and vanishes at infinity. Consequently, the outcome is real and we have no boundary term so that we can write
\begin{equation}
	\braket{\psi|\psi'} = \int \psi^*\psi' d^3\phi = -\int \psi'^*\psi d^3\phi = -\braket{\psi'|\psi} = -\braket{\psi|\psi'}^*.
\end{equation}
This represents the contribution to the matrix element of $M_r$ and we may conclude that it has no real part and should thus be zero. Consequently only the spin derivative matters and we may write
\begin{equation}
	\frac{d\ket{\Psi}}{dx} = \psi\frac{d}{dx}\ket{S}.	
\end{equation}





\subsection{r-derivative}



%The derivative of the wave function $d\psi/dr$ is is related to aspects. Among them\footnote{Other aspects that the axis of the harmonic oscillator and the dispersion of the spins changes.} are the change of mean value and of the width in the z-direction of the distribution function, as we already have seen in figure \ref{fig:overlap}.
%However, all changes are antisymmetric  and in calculating the connection the contribution is collapsed with the symmetric eigenstate and this will result in a zero contribution.

For the radial derivative we use the chain rule and \eqref{eq:SpinStates}, and obtain
\begin{equation} \label{eq:SpinDer}
	\frac{d}{dr} \ket{S_\pm} = \frac{d\theta}{dr} \frac{d}{d\theta} \ket{S_\pm}
	= \pm \frac{d\theta}{dr} \frac{i\sigma_y}{2} \ket{S_\pm} .
\end{equation}
The derivative $d\theta/dr$ can be expressed in coordinates of the minimum and consequently in the parameters. We use the chain rule
\begin{equation} \label{eq:ChainRule}
	\frac{d}{dr}\cos\theta = -\sin\theta \frac{d\theta}{dr},
\end{equation}
and fill in the components of $\vec\phi^0$ (figure \ref{fig:spinCond})
\begin{equation} \label{eq:PhiComp}
\begin{split}
	\phi^0_r&=|\vec\phi^0|\cos{\theta} \\
	\phi^0_z&=|\vec\phi^0|\sin{\theta} .
\end{split}	
\end{equation}
enabling us to express the derivative in coordinates of the minimum as
\begin{equation}
	\frac{d\theta}{dr} = - \frac{d\cos\theta/dr}{\sin\theta} = - \frac{d\phi^0_z/dr}{\phi^0_r} .
\end{equation}
To switch to parameter coordinates it is convenient to note that
\begin{equation}
	\frac{d\phi^0_z}{dr} = \frac{-1}{1-L_r/L_z} \frac{\phi^0_r}{\phi^0_z} ,
\end{equation}
so we finally arrive at an appropriate formula
\begin{equation}
	\frac{d\theta}{dr} = \frac{1}{1-L_r/L_z} \frac{1}{\phi^0_z} .
\end{equation}
Substituting this in \eqref{eq:SpinDer} we can write

\begin{equation}
	\frac{d}{dr} \ket{S_\pm} = \pm \frac{i\sigma_y}{2\phi^0_z(1-L_r/L_z)} \ket{S_\pm}
	= \pm \frac{i\sigma_y} {2\sqrt{(L_z-L_r)^2-r^2}} \ket{S_\pm} .
\end{equation}
This means we need to get the $\sigma_y$-matrix in our $\ket{S_\pm}$ basis, which turns out to be the same as in the quasi-spin basis. The components are readily evaluated to be

\begin{equation}
\begin{split}
	\braket{S_+|\sigma_y|S_+} &= \braket{\uparrow| e^{-i\sigma_y\theta/2} \sigma_y e^{i\sigma_y\theta/2} |\uparrow}
	= \braket{\uparrow| \sigma_y |\uparrow} = 0\\
	\braket{S_-|\sigma_y|S_-} &= \braket{\downarrow| e^{i\sigma_y\theta/2} \sigma_y e^{-i\sigma_y\theta/2} |\downarrow}
	= \braket{\downarrow| \sigma_y |\downarrow} = 0 \\
	\braket{S_+|\sigma_y|S_-} &=  \braket{\uparrow| e^{-i\sigma_y\theta/2} \sigma_y e^{-i\sigma_y\theta/2} |\downarrow}
	= \braket{\uparrow| \cos(\theta) \sigma_y -i\sin(\theta) \mathds{1} |\downarrow}
	= -i\cos\theta \\
	\braket{S_-|\sigma_y|S_+} &= \braket{\downarrow| e^{i\sigma_y\theta/2} \sigma_y e^{i\sigma_y\theta/2} |\uparrow}
	= \braket{\downarrow| \cos(\theta) \sigma_y +i\sin(\theta) \mathds{1} |\uparrow}
	= i\cos\theta .
\end{split}
\end{equation}
The end result is the first matrix of equation \eqref{eq:dPsidt}, reading

\begin{equation} \label{eq:Mr}
\begin{split}
	M_r &= \frac{1}{ 2\sqrt{(L_z-L_r)^2-r^2} } \begin{bmatrix} 0 & -\cos\theta \braket{\psi_-|\psi_+} \\ -\cos\theta \braket{\psi_+|\psi_-} & 0 \end{bmatrix} .
\end{split}
\end{equation}
For non-overlapping wave functions the connection in radial direction becomes zero.





\subsection{Tangential derivative}
\label{sc:phiDer}



The derivative w.r.t. $\varphi$ is less involved. Our spin state as a function of $\varphi$ is $\ket{S} = e^{i\sigma_z\varphi/2}\ket{S}_{\varphi=0}$, where as before the exponent rotates the spin. The derivative is

\begin{equation} \label{eq:thDerSpin}
	\frac{d}{d\varphi}\ket{S} = \frac{d}{d\varphi}e^{i\sigma_z\varphi/2}\ket{S}_{\varphi=0} = i\sigma_z/2 \ket{S} .
\end{equation}
This leaves the simple expression
\begin{equation} \label{eq:thDer2}
\begin{split}
	\frac{d\ket{\Psi}}{d\varphi} &= i\sigma_z/2 \ket{\Psi}.
\end{split}
\end{equation}
This is in agreement with the result \eqref{eq:M_adiabatic_appr} for isotropic circular trajactories. Now we calculate the connection. First we need to see that, using \eqref{eq:spinExp},

\begin{equation}
\begin{split}
\braket{S_+|\sigma_z|S_+} &= -\cos\theta \\
\braket{S_-|\sigma_z|S_-} &= \cos\theta \\
\braket{S_+|\sigma_z|S_-} &= \braket{\downarrow|\sigma_z|\uparrow} = 0 \\
\braket{S_-|\sigma_z|S_+} &= \braket{\uparrow|\sigma_z|\downarrow} = 0 .
\end{split}
\end{equation}
At the last two lines  we have used the definition of the exponent of $\sigma_y$ and used that this commutes with $\sigma_z$. The result for the connection in tangential direction is
\begin{equation} \label{eq:Mtheta}
	M_\varphi = \frac{i}{2} \begin{bmatrix} -\cos\theta & 0 \\ 0 & \cos\theta \end{bmatrix} .
\end{equation}
This means that there will be no mixing, when moving in the tangential direction. However, an opposite phase factor is aqcuired by the states $\ket{\psi_\pm}\ket{S_\mp}$, which are recognised as cyclic. This phase will be vary for different trajectories, since there is a dependence on $\theta$. This angle will change when $r$ is varied as seen in section \ref{sc:phiDer}, or possibly also when the offset $\phi^r_z$ is changed as the minimum tends to follow the reservoir phases. We conclude that our results are in very good agreement with chapter \ref{ch:Gen_Ana} and \ref{ch:Gen_Num}.

Although there is no mixing in the cyclic basis, by taking a different measurement basis it is possible to rotate around a different axis on the Bloch sphere. This is how more general manipulations could become possible. For example, the even and odd states, $(\ket{+}+\ket{-})$ and ($\ket{+}-\ket{-}$), the rotation axis will lie on the equator and the phase of cyclic basis will be translated into only occupation changes. For the mentioned possible basis of ground and excited state another axis of rotation might become available. 
%We transform for $\theta_0=\theta(t=0)$ using the trivial relations

%We need this matrix in the measurement spin basis, so we calculate it's elements using \eqref{eq:cyclic_states}. Below we use the Pauli matrices $\sigma_{x,y,z}$ in $\ket{S_\pm}$ basis and 
%\begin{equation}
%	e^{i\sigma_y\theta/2} \sigma_z e^{i\sigma_y\theta/2} = \sigma_z
%\end{equation}
%and find
%\begin{equation}
%\begin{split}
%	\braket{\uparrow|M|\uparrow} &= \frac{-i\cos\theta}{2} \braket{S_+ | e^{i\theta\sigma_y/2} \sigma_z e^{-i\theta\sigma_y/2} | S_+} \\
%	&= \frac{-i\cos\theta}{2} \braket{S_+ | \cos(\theta)\sigma_z -\sin(\theta)\sigma_x | S_+} \\
%	&= \frac{-i\cos^2\theta}{2} \\
%	\braket{\downarrow|M|\downarrow} &= \frac{-i\cos\theta}{2} \braket{S_- | e^{-i\theta\sigma_y/2} \sigma_z e^{i\theta\sigma_y/2} | S_-} \\
%	&= \frac{-i\cos\theta}{2} \braket{S_- | \cos(\theta)\sigma_z +\sin(\theta)\sigma_x | S_-} \\
%	&= \frac{i\cos^2\theta}{2} \\
%	\braket{\uparrow|M|\downarrow} &= \frac{-i\cos\theta}{2} \braket{S_+ | e^{i\theta\sigma_y/2} \sigma_z e^{i\theta\sigma_y/2} | S_-} \\
%	&= \frac{-i\cos\theta}{2} \braket{S_+ | \sigma_z | S_-} \\
%	&= 0 \\
%	\braket{\downarrow|M|\uparrow} &= \frac{-i\cos\theta}{2} \braket{S_- | e^{-i\theta\sigma_y/2} \sigma_z e^{-i\theta\sigma_y/2} | S_+} \\
%	&= \frac{-i\cos\theta}{2} \braket{S_- | \sigma_z | S_+} \\
%	&= 0 .
%\end{split}
%\end{equation}
%
%\begin{equation}
%\begin{split}
%	\ket{S_+} &= \cos{\theta_0/2}\ket{\uparrow} - \sin{\theta_0/2}\ket{\downarrow} \\
%	\ket{S_-} &= \cos{\theta_0/2}\ket{\downarrow} + \sin{\theta_0/2}\ket{\uparrow}
%\end{split}
%\end{equation}
%and rewriting this it is possible to deduce the inverse relations
%\begin{equation}
%\begin{split}
%	\ket{\uparrow} &= \cos{\theta_0/2}(\ket{S_+} + \tan{\theta_0/2}\ket{S_-}) = \cos{\theta_0/2}\ket{S_+} + \sin{\theta_0/2}\ket{S_-} \\
%	\ket{\downarrow} &= \cos{\theta_0/2}\left(\ket{S_-} - \tan{\theta_0/2}\ket{S_+}\right) = \cos{\theta_0/2}\ket{S_-} - \sin{\theta_0/2}\ket{S_+} .
%\end{split}
%\end{equation}
%These represent a unitary operator and its Hermitian conjugate. The end result is the evolution matrix in the desired basis
%\begin{equation} \label{eq:Ufinal}
%\begin{split}
%	U &= Pe^{\int_0^\tau M dt}P^{-1} \\
%	&= \begin{bmatrix} \cos(\theta_0/2) & \sin(\theta_0/2) \\ -\sin(\theta_0/2) & \cos(\theta_0/2) \end{bmatrix}
%	\begin{bmatrix} e^{-i\delta} & 0 \\ 0 & e^{i\delta} \end{bmatrix}
%	\begin{bmatrix}	\cos(\theta_0/2) & -\sin(\theta_0/2) \\ \sin(\theta_0/2) & \cos(\theta_0/2) \end{bmatrix} \\
%%	&= \begin{bmatrix} \cos^2(\theta_0/2)e^{-i\delta}+\sin^2(\theta_0/2)e^{i\delta} & \frac{1}{2}\sin{\theta_0}(e^{i\delta}-e^{-i\delta}) \\ \frac{1}{2}\sin{\theta_0}(e^{i\delta}-e^{-i\delta}) & \cos^2(\theta_0/2)e^{i\delta}+\sin^2(\theta_0/2)e^{-i\delta}  \end{bmatrix} \\
%	&= \begin{bmatrix} \cos(\delta)-i\cos(\theta_0)\sin(\delta) & i\sin{\theta_0}\sin(\delta) \\ i\sin{\theta_0}\sin(\delta) & \cos(\delta)+i\cos(\theta_0)\sin(\delta) \end{bmatrix} ,
%%	&= \begin{bmatrix} e^{-i\int\cos\theta d\varphi/2}+\sin^2(\theta/2)\sin(\int\cos\theta d\varphi/2) & \frac{1}{2}\sin{\theta}\sin(\int\cos\theta d\varphi/2) \\ \frac{1}{2}\sin{\theta}\sin(\int\cos\theta d\varphi/2) & e^{i\int\cos\theta d\varphi/2}-\sin^2(\theta/2)\sin(\int\cos\theta d\varphi/2) \end{bmatrix}
%% p. 386
%\end{split}
%\end{equation}
%where $\delta=\int\cos[\theta(r(\varphi))] \frac{d\varphi}{dt} \,dt/2$ for some $r(\varphi)$ determined by the trajectory. The dependence $\theta(r)$ is elaborated on in the next section. What this matrix does is rotate over an angle $2\delta$ determined by the geometric phase, around an axis at an angle $\theta_0$ w.r.t. the $z$-axis determined by the measurement basis. This provides control over the qubit operation performed. In the next chapter this connection is evaluated for several cases and compared to the numerical results of chapter \ref{ch:Gen_Num}. \towrite
%
%
%%The connection in our basis reads
%%\towrite{}
%%\begin{equation}
%%\begin{split}
%%	M_\varphi &= \frac{i}{2}
%%	\begin{bmatrix}	\cos{\theta/2} & \sin{\theta/2} \\ -\sin{\theta/2} & \cos{\theta/2} \end{bmatrix}
%%	\begin{bmatrix} -\cos\theta & 0 \\ 0 & \cos\theta \end{bmatrix} 
%%	\begin{bmatrix}	\cos{\theta/2} & -\sin{\theta/2} \\ \sin{\theta/2} & \cos{\theta/2} \end{bmatrix} \\
%%	&= \frac{i}{2} \cos{\theta}
%%	\begin{bmatrix}	\cos{\theta/2} & \sin{\theta/2} \\ -\sin{\theta/2} & \cos{\theta/2} \end{bmatrix}
%%	\begin{bmatrix}	-\cos{\theta/2} & \sin{\theta/2} \\ \sin{\theta/2} & \cos{\theta/2} \end{bmatrix} \\
%%	&= \frac{i}{2} \cos{\theta}
%%	\begin{bmatrix}	-\cos{\theta} & \sin{\theta} \\ \sin{\theta} & \cos{\theta} \end{bmatrix}
%%\end{split}
%%\end{equation}
%%which allows to implement any gate one wishes?

