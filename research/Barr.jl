# Plots the barrier height between the wells,
# the energy quanta of the two wells and
# the ratio between them
# as function of radius or Lz


using Printf
using LinearAlgebra
import Plots

# mode 1: vary Lz
# mode 2: vary r
mode = 2;

Nr = 201;
Nz = 401;
Nres = 41;

Lz = 4.0; Lr = 1.3;
Iz = 0.32; Ir = 0.24;
C = 100;
r = 0.3;
p = Lz/Lr-1;

if mode==1
    Lz_min = (Lr*Ir^2+r*Ir)/Iz^2;   # Condition for having two minima
    Lz_min = max(Lz_min, Lr);    # condition for positive p
    Lz_max = ceil(Lz_min)+10;
    LLz = range(Lz_min+0.01, stop=Lz_max, length=Nres);
    phiz = range(-sqrt((LLz[Nres]*Iz)^2 - (Ir/Iz*r / (1-Lr*Ir^2/(LLz[Nres]*Iz^2)) )^2)-1, stop=0, length=Nz);
    phir = range(r-1, stop=r+1, length=Nr);
elseif mode==2
    r_max = abs(Lz*Iz^2-Lr*Ir^2)/Ir;    # Condition for having two minima
    r_max = min(r_max, 1.0);      # Condition for positive eigenvalues of Hessian matrix
    rr = range(0, stop=r_max-0.01, length=Nres);
    phir = range(0, stop=r_max+1, length=Nr);
    phiz = range(-10, stop=0, length=Nz);
end


E_max = zeros(Nres);
E_max2 = zeros(Nres);
E_min = zeros(Nres);
E_min2 = zeros(Nres);
ff = zeros(Nr, Nz);
Eb = zeros(Nres,3);
hw = zeros(Nres);
hw2 = zeros(Nres);
ratio = zeros(Nres);
for m in 1:Nres
    if mode==1
        global r;
        Lz = LLz[m];
        p = Lz/Lr-1;
    elseif mode==2
        global Lz, p;
        r = rr[m];
    end
    
    # Analytically
    E_min[m] = 1/2*( (Ir*r)^2/(Lr*Ir^2-Lz*Iz^2) -Lz*Iz^2 );
    E_max[m] = Lr*Ir^2/2*(Ir*r)^2/(Lr*Ir^2-Lz*Iz^2)^2 - abs(Lz*Iz^2*(Ir*r)/(Lr*Ir^2-Lz*Iz^2));
    Eb[m,3] = E_max[m]-E_min[m];
    
    
    # Verification numerically
    f(phir,phiz) = (phir - r)^2 /(2*Lr) + phiz^2 /(2*Lz) - sqrt(Ir^2 *phir^2 + Iz^2 *phiz^2);
    for i in 1:Nr
        for j in 1:Nz
            ff[i,j] = f(phir[i], phiz[j]);
        end
    end
    I = findmin(ff)[2]
    
    println(@sprintf("minimum at [phir,phiz]           = [%.3g,%.3g]", phir[I[1]], phiz[I[2]]));
    phi1 = r/(1-Lr*Ir^2/(Lz*Iz^2))
    phi2 = -sqrt((Lz*Iz)^2 - (Ir/Iz*r / (1-Lr*Ir^2/(Lz*Iz^2)) )^2);
    println(@sprintf("minimum should be at [phir,phiz] = [%.3g,%.3g]", phi1, phi2));
    E_min2[m] = ff[I];
    E_max2[m] = ff[I[1],Nz];
    Eb[m,1] = E_max2[m]-E_min2[m];
    Eb[m,2] = f(phi1,0) - f(phi1,phi2);
    
    # Verification numerically
    
    # Oscillator well width
    Hes = [p+r^2 r*sqrt(1-r^2+0im); r*sqrt(1-r^2+0im) 1-r^2]/Lz;
    D = p^2+(-2+4*r^2)*p+1;     # Discriminant
    ev = real(eigvals(Hes));
    ev2 = [(p+1-sqrt(D))/2, (p+1+sqrt(D))/2]/Lz;
    println(ev);
    println(ev2);
    println();
    hw2[m] = sum(sqrt.(ev2*2/C))/2;     # Analytically
    hw[m] = sum(sqrt.(ev*2/C))/2;       # Numerically
    
    ratio[m] = Eb[m]/hw[m];
    
end

if mode==1
    ttl = @sprintf("Lr=%.2g, Iz=%.2g, Ir=%.2g, r=%.2g", Lr, Iz, Ir, r);
    var = "Lz";
    x = LLz;
elseif mode==2
    ttl = @sprintf("Lz=%.2g, Lr=%.2g, Iz=%.2g, Ir=%.2g", Lz, Lr, Iz, Ir);
    var = "r";
    x = rr;
end    

#Plots.pyplot();
#Plots.plot(phir, phiz, f, st=:surface, camera=(-30,30), show=true)
Plots.gr();
plt1 = Plots.plot(x, Eb, ylabel="Eb", xlabel=var, leg=false)
plt2 = Plots.plot(x, [hw hw2], ylabel="hw", xlabel=var, leg=false)
plt3 = Plots.plot(x, ratio, ylabel="Eb/hw", xlabel=var, leg=false)
Plots.plot(plt1, plt2, plt3, layout=(3,1), title=ttl);
Plots.savefig(@sprintf("Ebhw(%s) - %s", var, ttl));

# Verification plots
Plots.plot(x, E_max, labels="E_max", lw=5);
Plots.plot!(x, E_max2, labels="E_max2", lw=2);
Plots.plot!(x, E_min, labels="E_min", lw=5);
Plots.plot!(x, E_min2, labels="E_min2", lw=2, show=true);

