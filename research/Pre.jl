#import Pkg
using LinearAlgebra
using SparseArrays
using Arpack
using Printf
#using ProgressMeter
#import Plots
import Winston

struct ParametersJ
    hw::Tuple{Float64,Float64,Float64}
    I::Tuple{Float64,Float64,Float64}
    phi::Tuple{Float64,Float64,Float64}
end

const sigmax = [0 1;1 0] 
const sigmay = [0 -1im;1im 0]
const sigmaz = [1 0;0 -1] 

δ(m,n) = m==n ? 1 : 0



function printmat(mat)
	for j in 1:size(mat)[2]
		print("-------------");
	end
	println();
	for i in 1:size(mat)[1]
		if typeof(mat[1])==Complex{Float64}
			for j in 1:size(mat)[2]
				@printf("%82g   | ", real(mat[i,j]));
			end
			println();
			for j in 1:size(mat)[2]
				@printf("%82gim | ", imag(mat[i,j]));
			end
		else
			for j in 1:size(mat)[2]
				@printf("%82g   | ", mat[i,j]);
			end
		end
		println();
		for j in 1:size(mat)[2]
			print("-------------");
		end
		println();
	end
	println();
end


function calcEV(p::ParametersJ, NEV, N)
# Calculates two lowest eigenvactors and fixes phase

	global FillOperator;
	global H;
	Hm = FillOperator((l_,m_,n_,a_,l,m,n,a)-> abs(l-l_)+abs(m-m_)+abs(n-n_) > 1 ? 0 : H(l_,m_,n_,a_,l,m,n,a,p), N...);
	sol = eigs(Hm, nev=NEV, tol=1e-18, maxiter=10000, which=:SR);
	minus = transpose(sol[2][:,2]);
	plus = transpose(sol[2][:,1]);
	# Keep first component real
	minus *= abs(minus[1])/minus[1];
	plus *= abs(plus[1])/plus[1];
	EE = real(sol[1]);
	
	return minus, plus, EE;
end


function CalcRotation(A)
# Takes A as in U=exp^(-iA) and 
	globalphase = -(A[1,1]+A[2,2])/2;
	A = A+[1 0; 0 1]*globalphase;
	alpha = sqrt((2*A[1,1])^2+(A[1,2]+A[2,1])^2+(im*A[1,2]-im*A[2,1])^2);
	nz = - A[1,1]*2/alpha;
	nx = -(A[1,2]+A[2,1])/alpha;
	ny = -(im*A[1,2]-im*A[2,1])/alpha;
	nr = sqrt(nx^2+ny^2);
	
	return alpha, nr, nz;
end


function calcSpin(plus, minus)
    # Calculate spin
    spin_plus_x = plus*sigx*plus';
    spin_plus_y = plus*sigy*plus';
    spin_plus_z = plus*sigz*plus';
    spin_plus = [spin_plus_x, spin_plus_y, spin_plus_z];

    spin_minus_x = minus*sigx*minus';
    spin_minus_y = minus*sigy*minus';
    spin_minus_z = minus*sigz*minus';
    spin_minus = [spin_minus_x, spin_minus_y, spin_minus_z];

    spin_plusminus_x = plus*sigx*minus';
    spin_plusminus_y = plus*sigy*minus';
    spin_plusminus_z = plus*sigz*minus';

    spin_plusminus = [spin_plusminus_x, spin_plusminus_y, spin_plusminus_z];
    
    return spin_minus, spin_plus, spin_plusminus
end


function makeTrajectory(TR, a, b, offset, Ntr)

	global II, homega;
	if TR==1
	# Trajectory cyclic in x,y-plane
		a = 1;
		b = 1;
		theta = range(0, stop=2*pi, length=Ntr);
		p = Array{ParametersJ}(undef, Ntr);
		for i in 1:Ntr
			phi = [a*cos(theta[i]), b*sin(theta[i]), offset];
			p[i] = ParametersJ(homega,II,(phi,));
		end
	elseif TR==2
	# Trajectory elliptic in y,z-plane
		a = 1;
		b = 01;
		theta = range(0, stop=2*pi, length=Ntr);
		p = Array{ParametersJ}(undef, Ntr);
		for i in 1:Ntr
			phi = [offset a*cos(theta[i]) b*sin(theta[i])];
			p[i] = ParametersJ(homega,II,(phi,));
		end
	end
	
	return p;
end


