# Plots.gr();
# Numerical parameters
#global N = 20;
#N = (N,N,N);							# Number of basis vectors

#include("Prepare_system.jl");


AAA = AA(N);
Lz = L_z(N);
sigz = sig_z(N);
sigy = sig_y(N);
sigx = sig_x(N);




EE = zeros(Nphi,NEV);
A = zeros(2,2)*im;
A1 = zeros(2,2)*im;
A2 = zeros(2,2)*im;
nz = zeros(Nphi)*im; nz1 = zeros(Nphi)*im; nz2 = zeros(Nphi)*im;
nr = zeros(Nphi)*im; nr1 = zeros(Nphi)*im; nr2 = zeros(Nphi)*im;
alpha = zeros(Nphi)*im; alpha1 = zeros(Nphi)*im; alpha2 = zeros(Nphi)*im;
spin_minus = zeros(Nphi,3);
spin_plus = zeros(Nphi,3);

#	phi[4-Indx] = var2;
	
# Calculate unitary evolution for one cycle
EE, A, nz, nz1, nz2, nr, nr1, nr2, alpha, alpha1, alpha2, spin_minus, spin_plus;
spin_plus;
for ss in 1:Nphi
    println(string(ss, " "));
    global A, A1, A2, EE;
    
    phi[Indx] = phix[ss];
    p = ParametersJ(homega,II,(phi...,));
    
    minus, plus, EE[ss,:] = calcEV(p, NEV, N)
    
    # Calculate 2x2 matrix A as in U=exp^(-iA) in basis {|+>,|->}
    A = 2*pi*[ plus*AAA*plus' plus*AAA*minus' ; minus*AAA*plus' minus*AAA*minus' ];
    A1 = 2*pi*[ plus*Lz*plus' plus*Lz*minus' ; minus*Lz*plus' minus*Lz*minus' ];
    A2 = 2*pi*[ plus*sigz*plus' plus*sigz*minus' ; minus*sigz*plus' minus*sigz*minus' ];
    
    # Calculate rotation angles (for L_z and sigma/2 together)
    alpha[ss], nr[ss], nz[ss] = CalcRotation(A);
    # Calculate rotation angles for L_z and sigma/2 independently
    alpha1[ss], nr1[ss], nz1[ss] = CalcRotation(A1);
    alpha2[ss], nr2[ss], nz2[ss] = CalcRotation(A2);

    # Calculate spin orientation
    spin_minus[ss,:], spin_plus[ss,:] = calcSpin(plus, minus)
    
    println(nr[ss]);
    U = exp(-im*A);
    printmat(U*U');
end



