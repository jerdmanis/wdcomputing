N = 20;
N = (N,N,N);   # Number of basis vectors
NEV = 2
omega = 1;


function calcA(p, NEV, N)

		Ntr = length(p);
		h = (2*pi)/omega/(Ntr-1);
		M = zeros(2,2);
		MM = zeros(2,2);
		
		# Go around disc and calculate M
		minus1, plus1 = calcEV(p[1], NEV, N);
		for i in 2:Ntr
			minus2, plus2 = calcEV(p[i], NEV, N);
			M = [(plus1*plus2'-1)/h plus1*minus2'/h; minus1*plus2'/h (minus1*minus2'-1)/h] ;
			MM -= M*h;
			minus1, plus1 = minus2, plus2;
			print("|");
		end

		A = -im*MM;
		return A;
end



Nphi = 21;
traj = 1;
Indx = 2:3;
if Indx == [4]			# offset on x-axis
	phix = range(0, stop=0.003, length=Nphi);	
	radius = 1.0;
	paraTraj = [traj, radius, radius, phix[1]];
elseif Indx == 2:3		# radius on x-axis
	phix = range(0, stop=4, length=Nphi);
	phix = [phix phix];
	offset = 0.001;
	paraTraj = [traj, phix[1], phix[1], offset];
end

include("Prepare_system.jl");



# Calculate unitary evolution for one cycle
A = zeros(2,2)*im;
nz = zeros(Nphi)*im;
nr = zeros(Nphi)*im;
alpha = zeros(Nphi)*im;
Ntr = 51;
for ss in 1:Nphi
	global A, NEV, N, Ntr;
	println(string(ss, " "));

	# Calculate A for trajectory
	paraTraj[Indx] = phix[ss,:];
	p = makeTrajectory(paraTraj..., Ntr);
	A = calcA(p, NEV ,N);
	
	println(@sprintf("Lz=%.2g, Lr=%.2g, Iz=%.2g, Ir=%.2g, C=%.2g, offset=%.2g, N=%d", L[3], L[1], II[3], II[1], CC, p[1].phi[3], N[1])); #!!!
	
	# Calculate rotation angles
	alpha[ss], nr[ss], nz[ss] = CalcRotation(A);
	
	U = exp(-im*A);
	printmat(U*U');
	println(nr[ss]);
end


# Automate plot title
if Indx==[4]			# offset on x-axis
	ttl = @sprintf("Lz=%.2g, Lr=%.2g, Iz=%.2g, Ir=%.2g, C=%.2g, radius=%.2g, N=%d", L[3], L[2], II[3], II[2], CC, paraTraj[2], N[1]);
	var = "offset";
elseif Indx==2:3		# radius on x-axis
	ttl = @sprintf("Lz=%.2g, Lr=%.2g, Iz=%.2g, Ir=%.2g, C=%.2g, offset=%.2g, N=%d", L[3], L[2], II[3], II[2], CC, paraTraj[4], N[1]);
	var = "radius";
	phix = phix[:,1];
end

# Plot rotation axis
plt1 = Plots.plot(phix, abs.(nz), show=true, title=ttl, xlabel=var, ylabel="n_", label="nz");
Plots.plot!(phix, abs.(nr), label="nr");
Plots.savefig(@sprintf("Plot (%s) n(%s) (Num., Ntr=%d)", ttl, var, Ntr));

# Plot rotation angle
plt2 = Plots.plot(phix, abs.(alpha)/pi, show=true, title=ttl, xlabel=var, ylabel="alpha/pi", label="alpha");
Plots.savefig(@sprintf("Plot (%s) alpha(%s) (Num., Ntr=%d)", ttl, var, Ntr));