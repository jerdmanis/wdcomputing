# Prints 8x8 evolution matrix for 8 states
# as function of radius or offset.

N = 20;
N = (N,N,N);   # Number of basis vectors
NEV = 8;   # Number of eigenvectors

include("Prepare_system.jl");


# Choose for what values to make plots
Indx = 1;
Nphi = 11;
if Indx==1		# radius on x-axis
	phix = range(0, stop=4, length=Nphi);
	offset = 0;
	phi = [phix[1], 0.0, offset];
elseif Indx==3	# offset on x-axis
	radius = 1.0;
	phix = range(0, stop=0.006, length=Nphi);
	phi = [radius, 0.0, phix[1]];
end
println(phi[4-Indx]);

A = zeros(2,2)*im;
opm = zeros(2,2)*im;
EV = zeros(8, N[1]^3*2);
EE = zeros(Nphi,NEV);

for ss in 1:Nphi
	global A, opm;
	println(phix[ss]);
	
	phi[Indx] = phix[ss];
	p = ParametersJ(homega,II,(phi...,));
	
	Hm = FillOperator((l_,m_,n_,a_,l,m,n,a)-> abs(l-l_)+abs(n-n_)+abs(m-m_) > 1 ? 0 : H(l_,m_,n_,a_,l,m,n,a,p),N...);
	sol = eigs(Hm, nev=NEV, tol=1e-18, maxiter=10000, which=:SR);
	EE[ss,:] = real(sol[1]);
	EV = sol[2]';
	
	# Calculate 8x8 matrices A, H, U
	A8 = EV*(AA*EV');
	A8 = A8*2*pi;
	Hm8 = EV*(Hm*EV');
	U = exp(-im*A8)*exp(-im*(Hm8-A8));
	printmat(norm.(U));
end
