N = 10;
N = (N,N,N);							# Number of basis vectors
NEV = 4;								# Number of eigenvectors

include("Prepare_system.jl");
include("Pre.jl")
include("Ops.jl")

σx = sig_x(N)
σy = sig_y(N)
σz = sig_z(N)
ϕz = phi_z(N)


for z in range(-0.1,stop=0.1,length=10)

    @show z
    
    phi = (0.5,0.,z)

    p = ParametersJ(homega,II,(phi...,));
    minus, plus, E = calcEV(p, NEV, N)

    @show plus*σx*plus'
    @show minus*σx*minus'

    @show plus*σy*plus'
    @show minus*σy*minus'

    @show plus*σz*plus'
    @show minus*σz*minus'

    @show plus*ϕz*plus'
    @show minus*ϕz*minus'
    
    @show plus*minus'
    @show plus*σz*minus'
end
