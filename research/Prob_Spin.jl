Plots.gr();
# Numerical parameters
global N = 20;
N = (N,N,N);							# Number of basis vectors
NEV = 2;								# Number of eigenvectors

include("Prepare_system.jl");

phi = (1.0, 0.0, 0.5)
p = ParametersJ(homega,II,(phi...,));
minus, plus, EE[ss,:] = calcEV(p, NEV, N);

# Calculate polynomial coefficients for Hermite polynomials
Nvec = N[1]+1;
coef = zeros(Nvec+1,Nvec+1);
coef[1,1] = 1; 
coef[2,2] = 2; 
for m in 2:Nvec
    coef[m+1,:] = 2*[0; coef[(m-1)+1,1:end-1]]-2*(m-1)*coef[(m-2)+1,:]/(2*sqrt(2^m)*pi^(1/4));
end


index(l,m,n,a) = 2*(Ny+1)*(Nz+1)*l+2*(Nz+1)*m+2*n+a;
XZ = zeros(Nx+1, Nz+1);
for x in 1:Nvec+1
    for z in 1:Nvec+1
        for y in 1:Nvec+1
            for s in 1:2
                XZ[x,z] += minus(index(x,y,z,s))^2;
            end
        end
    end
end
XZ = sqrt(XZ);

for x in 1:Nvec+1
    for z in 1:Nvec+1
        
    end
end

EV = ?(minus/plus);

for n in Nvec
    coef = EV*coef; # EV
end

X(x) = [x^n for n in range(0, stop=Nvec-1)];

function HarOsc(n, x)
    return transpose(c[n+1,:])*X(x);
end

for phix in Phix
    for phiy in Phiy
        Prob = Harosc(coef, phix)
    end
end
